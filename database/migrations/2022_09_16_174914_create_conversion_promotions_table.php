<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversion_promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('promotion_id')->comment('promotion is synonym of classe');
            $table->string('promotion_label')->comment('promotion is synonym of classe. Have same value than code hyperplanning ');
            $table->string('implantation_id');
            $table->string('implantation_label');
            $table->string('orientation_id');
            $table->string('orientation_label');
            $table->string('annee');
            $table->string('classe')->nullable();
            $table->string('name_hyperplanning')->nullable();
            $table->string('key_hyperplanning')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversion_promotions');
    }
};
