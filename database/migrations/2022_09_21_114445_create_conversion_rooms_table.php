<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversionRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversion_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key_hyperplanning')->unique();
            $table->string('room_name')->comment('is code for hyperplanning');
            $table->string('implantation_prefix');
            $table->string('implantation_name');
            $table->string('implantation_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversion_rooms');
    }
}
