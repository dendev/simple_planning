<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversionTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversion_teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numproeco');
            $table->string('login');
            $table->string('name');
            $table->string('firstname');
            $table->string('email');
            $table->string('code_hyperplanning');
            $table->string('key_hyperplanning');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hyperplanning_mdps');
    }
}
