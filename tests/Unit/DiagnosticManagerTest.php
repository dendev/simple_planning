<?php

namespace Tests\Unit;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DiagnosticManagerTest extends TestCase
{
    use DatabaseMigrations;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $loader = AliasLoader::getInstance();
        $loader->alias('DiagnosticManager', '\App\Facades\DiagnosticManagerFacade');
    }

    //
    public function testBasic()
    {
        $exist = \DiagnosticManager::test_me();
        $this->assertEquals('diagnostic_manager', $exist);
    }
}
