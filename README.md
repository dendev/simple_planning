# Simple Planning
> gestion d'hyperplanning via une api rest

Permet de récupérer des horaires sous forme json.   
Réalise de la réconciliation de données entre Henallux et HP.   
Permet des routines de diagnostic pour vérifié la cohérence des données.

Ex: Je veux l'horaire d'un étudiant. Je veux savoir pourquoi un mdp n'as pas d'horaire.

## Installation

```bash
git clone https://gitlab.com/DTM-Henallux/SiegeCentral/dev/simple_planning.git
cd simple_planning 
composer install && npm install && npm run dev
vim .env
php artisant migrate --seed
```

## Utilisation
Des tables de conversion existent pour réconcilé la donnée Henallux et HP.    
Elle sont générés par les commandes dispo.    
```php
php artisan simple_planning:update
```

Des commandes de diagnostic sont dispo
```php
php artisan simple_planning:diagnostic
```

L'api permet la récuération de calendrier pour les promotions, étudiants, mdp, externe et classe.   
Des routes de diagnostic sont aussi disponible.  
Le tout est documenté sur [http://simple_planning.local/api/documentation](http://simple_planning.local/api/documentation) 

## Docs

### Php
Installer [doctum](https://github.com/code-lts/doctum)
```bash
curl -O https://doctum.long-term.support/releases/latest/doctum.phar
php doctum.phar update doctum.php
google-chrome ./docs/build/index.html
```

### Api
Dispo [http://simple_planning.local/api/documentation](http://simple_planning.local/api/documentation) 

## Test
Executer tous les tests
```bash
phpunit 
```

Executer un jeu de test spécifique
```bash
phpunit tests/Unit/DiagnosticManagerTest.php
```

Executer un test spécifique
```bash
phpunit --filter testBasic
```

## Metrics

Installer [phpmetrics](https://phpmetrics.org/)
```bash
composer global require 'phpmetrics/phpmetrics'
phpmetrics --report-html=metrics ./src --junit='phpunit.xml'
google-chrome metrics/index.html
```

## TODO
diagnostic_teacher_code get nomEnseignants -> check nom + prenom == code
