<?php

return [
    'october_day_label' => "Jour d'octobre",
    'october_day_hint' => "Jour de basculement en heure d'hiver",
    'march_day_label' => "Jour de mars",
    'march_day_hint' => "Jour de basculement en heure d'été",
];
