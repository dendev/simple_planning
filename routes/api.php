<?php

use App\Http\Controllers\Api\Diagnostic;
use App\Http\Controllers\Api\Extern;
use App\Http\Controllers\Api\Promotion;
use App\Http\Controllers\Api\Room;
use App\Http\Controllers\Api\Teacher;
use App\Http\Controllers\Api\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/promotions', function (Request $request) {
    $promotions = \PlanningManager::get_all_promotions();

    return $promotions;
});

// calendars
Route::get('/promotions/calendar/{key}/{nb_weeks?}', [Promotion::class, 'get_calendar']);
Route::get('/students/calendar/{keys}/{nb_weeks?}', [Student::class, 'get_calendar']);
Route::get('/teachers/calendar/{keys}/{nb_weeks?}', [Teacher::class, 'get_calendar']);
Route::get('/externs/calendar/{keys}/{nb_weeks?}', [Extern::class, 'get_calendar']);
Route::get('/rooms/calendar/{keys}/{nb_weeks?}', [Room::class, 'get_calendar']);

// icals
Route::get('/rooms/ical/{key}/{nb_weeks?}', [Room::class, 'get_ical']);

// diagnostics
Route::get('/diagnostic/promotions/{name}/{fake?}', [Diagnostic::class, 'get_promotion']);
Route::get('/diagnostic/promotions/{fake?}/{json?}', [Diagnostic::class, 'get_promotions']);

Route::get('/diagnostic/students/{numproeco}/{fake?}', [Diagnostic::class, 'get_student']);
Route::get('/diagnostic/students', [Diagnostic::class, 'get_students']);

Route::get('/diagnostic/teachers/{login}/{fake?}', [Diagnostic::class, 'get_teacher']);
Route::get('/diagnostic/teachers', [Diagnostic::class, 'get_teachers']);

Route::get('/diagnostic/externs/{login}/{fake?}', [Diagnostic::class, 'get_extern']);
Route::get('/diagnostic/externs', [Diagnostic::class, 'get_externs']);

Route::get('/diagnostic/rooms/{name}/{fake?}', [Diagnostic::class, 'get_room']);
Route::get('/diagnostic/rooms', [Diagnostic::class, 'get_rooms']);

