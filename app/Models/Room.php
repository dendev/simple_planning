<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Room extends Model // Room is alias model he use conversion_rooms too !
{
    public $table = 'conversion_rooms';

    protected $fillable = [
        'key_hyperplanning',
        'room_name',
        'implantation_prefix',
        'implantation_name',
        'implantation_id'
    ];
}
