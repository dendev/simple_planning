<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Extern extends Model
{
    public $connection = 'sheldon';
    public $table = 'externes_he';


    public function get_hp_code()
    {
        $hp_code = strtoupper($this->abreviation);

        return $hp_code;
    }
}

