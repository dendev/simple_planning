<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversionTeacher extends Model
{
    public $table = 'conversion_teachers';

    protected $fillable = [
        'numproeco',
        'login',
        'name',
        'firstname',
        'email',
        'code_hyperplanning',
        'key_hyperplanning',
    ];

}

