<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Student extends Model
{
    public $connection = 'proecohenallux';
    public $table = 'etudiantsproeco';


    public function get_hp_name()
    {
        $hp_key = false;

        // Merci Gerard ! // get classe of student
        $sql = DB::connection('proecohenallux' )->select(
            "select c.intitule AS intitule, o.key_impl AS implantation_id, c.annee AS annee from etudiantsproeco e
		    inner join orientations o
		    on e.orientation = o.code and o.key_impl = e.key_impl
		    inner join classesproeco c
		    on e.key_impl = c.fkImplantation and e.classe = IFNULL(c.classe, 'X') and e.annee = c.annee and o.id = c.fkOrientation
		    where numproeco = ?",
            [$this->numproeco]
        );

        if( ! empty( $sql ) )
            $hp_key = $sql[0]->intitule;

        return $hp_key;
    }
}

