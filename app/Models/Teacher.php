<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Teacher extends Model
{
    public $connection = 'proecohenallux';
    public $table = 'profsproeco';


    public function get_hp_code()
    {
        $hp_code = str_replace('mdp', '', $this->login);
        $hp_code = strtoupper($hp_code);

        return $hp_code;
    }
}

