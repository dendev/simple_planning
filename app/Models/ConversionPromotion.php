<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversionPromotion extends Model
{

    public $table = 'conversion_promotions';

    protected $fillable = [
        'promotion_id',
        'promotion_label',
        'implantation_id',
        'implantation_label',
        'orientation_id',
        'orientation_label',
        'annee',
        'classe',
        'name_hyperplanning',
        'key_hyperplanning'
    ];

    // test for filter in admin
    public function scopeImplantation($query, $implantation_id)
    {
        return $query->where('implantation_id', $implantation_id);
    }
}
