<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversionExterne extends Model
{
    public $table = 'conversion_externes';

    protected $fillable = [
        'id_people',
        'login',
        'name',
        'firstname',
        'email',
        'code_hyperplanning',
        'key_hyperplanning',
    ];
}

