<?php
namespace App\Services;


use App\Services\Diagnostics\Extern;
use App\Services\Diagnostics\Promotion;
use App\Services\Diagnostics\Room;
use App\Services\Diagnostics\Student;
use App\Services\Diagnostics\Teacher;
use Illuminate\Support\Carbon;

class DiagnosticManagerService
{
    use Extern;
    use Room;
    use Teacher;
    use Promotion;
    use Student;

    private $_ref_promotion_keys;
    private $_ref_promotion_names;

    private $_ref_teacher_keys;
    private $_ref_teacher_codes;

    private $_ref_room_keys;
    private $_ref_room_names;

    public function test_me()
    {
        return 'diagnostic_manager';
    }

    public function init_promotion_refs()
    {
        $keys_promotion = \PlanningManager::ToutesLesPromotions();
        $names_promotion = \PlanningManager::NomsTableauDePromotions($keys_promotion);

        $this->_ref_promotion_keys = $keys_promotion;
        $this->_ref_promotion_names = $names_promotion;

        if( ! $keys_promotion || ! $names_promotion )
        {
            \Log::stack(['stack', 'diagnostic'])->error("[DiagnosticManagerStudent::init_promotions_refs] DMSipr01 : Error missing keys promotions or names promotions ( check hp access )", [
                'keys_promotion' => $keys_promotion,
                'names_promotion' => $names_promotion,
            ]);

            throw new \Exception('DSh01 : Error missing keys promotions or names promotions ( check hp access )');
        }
    }

    public function init_teacher_refs()
    {
        $keys_teacher = \PlanningManager::tousLesEnseignants();
        $codes_teacher = \PlanningManager::codesTableauEnseignants($keys_teacher);

        $this->_ref_teacher_keys = $keys_teacher;
        $this->_ref_teacher_codes = $codes_teacher;

        if( ! $keys_teacher || ! $codes_teacher )
        {
            \Log::stack(['stack', 'diagnostic'])->error("[DiagnosticManagerTeacher::init_teacher_refs] DMSit01 : Error missing keys teachers or codes teachers ( check hp access )", [
                'keys_teacher' => $keys_teacher,
            ]);

            throw new \Exception('DSh01 : Error missing keys teachers or codes teachers ( check hp access )');
        }
    }

    public function init_room_refs()
    {
        $keys_room = \PlanningManager::toutesLesSalles();
        $names_room = \PlanningManager::nomsTableauDeSalles($keys_room);

        $this->_ref_room_keys = $keys_room;
        $this->_ref_room_names = $names_room;

        if( ! $keys_room || ! $names_room )
        {
            \Log::stack(['stack', 'diagnostic'])->error("[DiagnosticManager::init_room_refs] DMirr01 : Error missing keys rooms or names rooms ( check hp access )", [
                'keys_room' => $keys_room,
                'names_room' => $names_room,
            ]);

            throw new \Exception('DMSirr01 : Error missing keys room or name room ( check hp access )');
        }
    }

     public function init_extern_refs()
    {
        $keys_extern = \PlanningManager::tousLesEnseignants();
        $codes_extern = \PlanningManager::codesTableauEnseignants($keys_extern);

        $this->_ref_extern_keys = $keys_extern;
        $this->_ref_extern_codes = $codes_extern;

        if( ! $keys_extern || ! $codes_extern )
        {
            \Log::stack(['stack', 'diagnostic'])->error("[DiagnosticManager::init_extern_refs] DMier01 : Error missing keys extern or codes extern ( check hp access )", [
                'keys_extern' => $keys_extern,
                'keys_codes' => $codes_extern,
            ]);

            throw new \Exception('DSh01 : Error missing keys or codes extern ( check hp access )');
        }
    }
}
