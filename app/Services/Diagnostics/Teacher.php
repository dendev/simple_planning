<?php

namespace App\Services\Diagnostics;

use App\Models\ConversionPromotion;
use App\Models\ConversionTeacher;
use App\Models\Teacher as TeacherModel;

trait Teacher
{
    private $_teacher_info;
    private $_teacher_debug = ['portail_magic_login' => 'https://portail.henallux.be/login?ignore'];

        /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function teacher_get_info()
    {
        return $this->_teacher_info;
    }

    public function teacher_get_debug()
    {
        return $this->_teacher_debug;
    }

    public function teacher_check_teachers_exist($teachers)
    {
        $this->_teacher_info['exist']['label'] = "Vérifie l'étudiant dans proeco";

        if (!$teachers->isEmpty())
        {
            $this->_teacher_info['exist']['is_valid'] = true;
            $this->_teacher_info['exist']['msg'] = "Mdp existe bien dans proecohenallux et est actif";
        }
        else
        {
            $this->_teacher_info['exist']['is_valid'] = false;
            $this->_teacher_info['exist']['msg'] = "Mdp n'existe pas dans proecohenallux ou n'est pas actif";
        }
    }

    public function teacher_init_debug_infos($teacher = false, $debug = [])
    {
        $debug['login'] = $teacher ? $teacher->login. '@henallux.be' : null;
        $debug['firstname'] = $teacher ? $teacher->nom : null;
        $debug['lastname'] = $teacher ? $teacher->prenom : null;
        $debug['numproeco'] = $teacher ? $teacher->numproeco : null;

        $this->_teacher_debug = $debug;
    }

    private function _teacher_check_teacher_hp_code($teacher)
    {
        $hp_code = $teacher->get_hp_code();

        $this->_teacher_debug['hp_code'] = $hp_code;

        $this->_teacher_info['has_hp_code']['label'] = "Vérifie le nom hyperplanning, issut de proeco, pour le mdp";

        if( $hp_code )
        {
            $this->_teacher_info['has_hp_code']['is_valid'] = true;
            $this->_teacher_info['has_hp_code']['msg'] = "Le mdp à un nom hp crée depuis proeco";
        }
        else
        {
            $this->_teacher_info['has_hp_code']['is_valid'] = false;
            $this->_teacher_info['has_hp_code']['msg'] = "Impossible de crée le nom hp du mdp depuis proeco";
        }

        return $hp_code;
    }

    private function _teacher_check_hp_code_exist_in_hp($hp_code)
    {
        $is_valid = false;

        $this->_teacher_debug['hp_code'] = $hp_code;

        if( in_array($hp_code, $this->_ref_teacher_codes) )
            $is_valid = true;

        $this->_teacher_info['has_hp_code_in_hp']['label'] = "Vérifie le nom hyperplanning du mdp dans HP";

        if( $is_valid)
        {
            $this->_teacher_info['has_hp_code_in_hp']['is_valid'] = true;
            $this->_teacher_info['has_hp_code_in_hp']['msg'] = "Le mdp à un nom qui existe dans hp";
        }
        else
        {
            $this->_teacher_info['has_hp_code_in_hp']['is_valid'] = false;
            $this->_teacher_info['has_hp_code_in_hp']['msg'] = "Le mdp n'a pas de nom qui existe dans hp";
        }

        return $is_valid;
    }

    private function _teacher_check_hp_code_exist_in_db($hp_code)
    {
        $is_valid = false;

        $this->_teacher_debug['hp_code'] = $hp_code;

        $conversion_teacher = ConversionTeacher::where('code_hyperplanning', $hp_code)->first();
        if( $hp_code && $conversion_teacher)
            $is_valid = true;

        $this->_teacher_info['has_hp_code_in_db']['label'] = "Vérifie le nom hyperplanning du mdp existe dans la DB de conversion";

        if( $is_valid)
        {
            $this->_teacher_info['has_hp_code_in_db']['is_valid'] = true;
            $this->_teacher_info['has_hp_code_in_db']['msg'] = "Le mdp à un nom qui existe dans la table conversion_teacher";
        }
        else
        {
            $this->_teacher_info['has_hp_code_in_db']['is_valid'] = false;
            $this->_teacher_info['has_hp_code_in_db']['msg'] = "Le mdp n'a pas de nom qui existe dans la table conversion_teacher";
        }

        return $is_valid;
    }

    private function _teacher_check_hp_key_for_hp_code($hp_code )
    {
        $is_valid = false;
        $key = false;

        $this->_teacher_debug['hp_code'] = $hp_code;

        $index =  array_search( $hp_code, $this->_ref_teacher_codes);
        if( $index !== false )
        {
            $key = $this->_ref_teacher_keys[$index];
            if( $key )
            {
                $is_valid = true;
            }
        }

        $this->_teacher_info['has_hp_key']['label'] = "Vérifie que le code mdp existe dans HP";

        if( $is_valid)
        {
            $this->_teacher_info['has_hp_key']['is_valid'] = true;
            $this->_teacher_info['has_hp_key']['msg'] = "Le mdp a une clé qui existe dans hp";
        }
        else
        {
            $this->_teacher_info['has_hp_key']['is_valid'] = false;
            $this->_teacher_info['has_hp_key']['msg'] = "Le mdp n'a pas de clé qui existe dans hp";
        }


        return $key;
    }

    private function _teacher_check_ical_for_hp_code($key_hp)
    {
        $is_valid = false;

        $this->_teacher_debug['key_hp'] = $key_hp;
        $this->_teacher_debug['nb_weeks'] = 53;

        $ical = \PlanningManager::icalPromotion($key_hp, 53);
        if( $ical )
            $is_valid = true;

        $this->_teacher_info['has_hp_ical']['label'] = "Vérifie que l'horaire existe dans HP";

        if( $is_valid )
        {
            $this->_teacher_info['has_hp_ical']['is_valid'] = true;
            $this->_teacher_info['has_hp_ical']['msg'] = "Le mdp a un ical qui existe dans hp";
        }
        else
        {
            $this->_teacher_info['has_hp_ical']['is_valid'] = false;
            $this->_teacher_info['has_hp_ical']['msg'] = "Le mdp n'a pas d'ical existe dans hp";
        }

        return $ical;
    }

    public function teacher_is_valid($info, $fake = false)
    {
        if (
            $info['exist']['is_valid'] &&
            $info['has_hp_code']['is_valid'] &&
            $info['has_hp_code_in_hp']['is_valid'] &&
            $info['has_hp_code_in_db']['is_valid'] &&
            $info['has_hp_key']['is_valid']
        )
        {
            if( $fake )
                $is_valid = true;
            else if( $this->_teacher_info['has_hp_ical']['is_valid'] )
                $is_valid = true;
            else
                $is_valid = false;
        }
        else
        {
            $is_valid = false;
        }

        return $is_valid;
    }

    public function teacher_run_diagnostic($teacher, $is_fake = false)
    {
        // initialise debug infos
        $this->teacher_init_debug_infos($teacher, $this->_teacher_debug);

        // check hp name created
        $hp_code = $this->_teacher_check_teacher_hp_code($teacher);

        // check hp name exist in hp
        $this->_teacher_check_hp_code_exist_in_hp($hp_code);

        // check hp nam exist in db
        $this->_teacher_check_hp_code_exist_in_db($hp_code);

        // check key hp
        $key_hp = $this->_teacher_check_hp_key_for_hp_code($hp_code);

        // check key hp in db

        // check ical
        if( ! $is_fake )
            $this->_teacher_check_ical_for_hp_code($key_hp);
    }


    public function teacher_get_results($infos, $debugs, $fake = false)
    {
        $valids = [];
        $errors = [];

        foreach( $infos as $key => $info)
        {
            $info['debug'] = $debugs[$key];

            $is_valid = $this->teacher_is_valid($info, $fake);
            if ($is_valid)
            {
                if( $fake )
                {
                    $valids[] = $info;
                }
                else if( $info['has_hp_ical']['is_valid'] )
                {
                    $valids[] = $info;
                }
                else
                {
                    $errors[] = $info;
                }
            }
            else
            {
                $errors[] = $info;
            }
        }

        $result['valids'] = $valids;
        $result['errors'] = $errors;

        return $result;
    }


    public function teacher_get_teachers($login = false)
    {
        if( $login )
        {
            $teachers = TeacherModel::where('actif', '1')
                ->where('login', $login)
                ->get();
        }
        else
        {
            $teachers = TeacherModel::where('actif', '1')
                ->get();
        }

        return $teachers;
    }
}
