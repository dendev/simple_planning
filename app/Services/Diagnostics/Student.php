<?php

namespace App\Services\Diagnostics;

use App\Models\ConversionPromotion;
use App\Models\Student as StudentModel;

trait Student
{
    private $_student_info;
    private $_student_debug = ['portail_magic_login' => 'https://portail.henallux.be/login?ignore'];

        /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function student_get_info()
    {
        return $this->_student_info;
    }

    public function student_get_debug()
    {
        return $this->_student_debug;
    }

    public function student_check_students_exist($students)
    {
        $this->_student_info['exist']['label'] = "Vérifie l'étudiant dans proeco";

        if (!$students->isEmpty())
        {
            $this->_student_info['exist']['is_valid'] = true;
            $this->_student_info['exist']['msg'] = "Etudiant(s) existe bien dans proecohenallux et est actif";
        }
        else
        {
            $this->_student_info['exist']['is_valid'] = false;
            $this->_student_info['exist']['msg'] = "Etudiant(s) n'existe pas dans proecohenallux ou n'est pas actif";
        }
    }

    public function student_init_debug_infos($student = false, $debug = [])
    {
        $debug['login'] = $student ? 'etu' . $student->numproeco . '@henallux.be' : null;
        $debug['firstname'] = $student ? $student->nom : null;
        $debug['lastname'] = $student ? $student->prenom : null;
        $debug['implantation'] = $student ? $student->key_impl : null;
        $debug['orientation'] = $student ? $student->orientation : null;
        $debug['bloc'] = $student ? $student->annee : null;
        $debug['class'] = $student ? $student->classe : null;
        $debug['numproeco'] = $student ? $student->numproeco : null;

        $this->_student_debug = $debug;
    }

    private function _student_check_student_hp_name($student)
    {
        $hp_name = $student->get_hp_name();

        $this->_student_debug['hp_name'] = $hp_name;

        $this->_student_info['has_hp_name']['label'] = "Vérifie le nom hyperplanning, issut de proeco, de la promotion ";

        if( $hp_name )
        {
            $this->_student_info['has_hp_name']['is_valid'] = true;
            $this->_student_info['has_hp_name']['msg'] = "L'étudiant à un nom d'orientation hp crée depuis proeco";
        }
        else
        {
            $this->_student_info['has_hp_name']['is_valid'] = false;
            $this->_student_info['has_hp_name']['msg'] = "Impossible de crée le nom d'orientation hp de l'étudiant depuis proeco";
        }

        return $hp_name;
    }

    private function _student_check_hp_name_exist_in_hp($hp_name)
    {
        $is_valid = false;

        $this->_student_debug['hp_name'] = $hp_name;

        if( in_array($hp_name, $this->_ref_promotion_names ) )
            $is_valid = true;

        $this->_student_info['has_hp_name_in_hp']['label'] = "Vérifie le nom hyperplanning de la promotion dans HP";

        if( $is_valid)
        {
            $this->_student_info['has_hp_name_in_hp']['is_valid'] = true;
            $this->_student_info['has_hp_name_in_hp']['msg'] = "L'étudiant a un nom de promotion qui existe dans hp";
        }
        else
        {
            $this->_student_info['has_hp_name_in_hp']['is_valid'] = false;
            $this->_student_info['has_hp_name_in_hp']['msg'] = "L'étudiant n'a pas de nom de promotion qui existe dans hp";
        }

        return $is_valid;
    }

    private function _student_check_hp_name_exist_in_db($hp_name)
    {
        $is_valid = false;

        $this->_student_debug['hp_name'] = $hp_name;

        $conversion_promotion = ConversionPromotion::where('name_hyperplanning', $hp_name)->first();
        if( $hp_name && $conversion_promotion)
            $is_valid = true;

        $this->_student_info['has_hp_name_in_db']['label'] = "Vérifie le nom hyperplanning de la promotion dans la DB de conversion";

        if( $is_valid)
        {
            $this->_student_info['has_hp_name_in_db']['is_valid'] = true;
            $this->_student_info['has_hp_name_in_db']['msg'] = "L'étudiant a un nom de promotion qui existe dans la table conversion_promotion";
        }
        else
        {
            $this->_student_info['has_hp_name_in_db']['is_valid'] = false;
            $this->_student_info['has_hp_name_in_db']['msg'] = "L'étudiant n'a pas de nom de promotion qui existe dans la table conversion_promotion";
        }

        return $is_valid;
    }

    private function _student_check_hp_key_for_hp_name($hp_name )
    {
        $is_valid = false;
        $key = false;

        $this->_student_debug['hp_name'] = $hp_name;

        $index =  array_search( $hp_name, $this->_ref_promotion_names );
        if( $index !== false )
        {
            $key = $this->_ref_promotion_keys[$index];
            if( $key )
            {
                $is_valid = true;
            }
        }

        $this->_student_info['has_hp_key']['label'] = "Vérifie que la clé de promotion existe dans HP";

        if( $is_valid)
        {
            $this->_student_info['has_hp_key']['is_valid'] = true;
            $this->_student_info['has_hp_key']['msg'] = "L'étudiant a une clé de promotion qui existe dans hp";
        }
        else
        {
            $this->_student_info['has_hp_key']['is_valid'] = false;
            $this->_student_info['has_hp_key']['msg'] = "L'étudiant n'a pas de clé de promotion qui existe dans hp";
        }


        return $key;
    }

    private function _student_check_ical_for_hp_name($key_hp)
    {
        $is_valid = false;

        $this->_student_debug['key_hp'] = $key_hp;
        $this->_student_debug['nb_weeks'] = 53;

        $ical = \PlanningManager::icalPromotion($key_hp, 53);
        if( $ical )
            $is_valid = true;

        $this->_student_info['has_hp_ical']['label'] = "Vérifie que l'horaire existe dans HP";

        if( $is_valid )
        {
            $this->_student_info['has_hp_ical']['is_valid'] = true;
            $this->_student_info['has_hp_ical']['msg'] = "L'étudiant a un ical qui existe dans hp";
        }
        else
        {
            $this->_student_info['has_hp_ical']['is_valid'] = false;
            $this->_student_info['has_hp_ical']['msg'] = "L'étudiant n'a pas d'ical existe dans hp";
        }

        return $ical;
    }

    public function student_is_valid($info, $fake = false)
    {
        if (
            $info['exist']['is_valid'] &&
            $info['has_hp_name']['is_valid'] &&
            $info['has_hp_name_in_hp']['is_valid'] &&
            $info['has_hp_name_in_db']['is_valid'] &&
            $info['has_hp_key']['is_valid']
        )
        {
            if( $fake )
                $is_valid = true;
            else if( $this->_student_info['has_hp_ical']['is_valid'] )
                $is_valid = true;
            else
                $is_valid = false;
        }
        else
        {
            $is_valid = false;
        }

        return $is_valid;
    }

    public function student_run_diagnostic($student, $is_fake = false)
    {
        // initialise debug infos
        $this->student_init_debug_infos($student, $this->_student_debug);

        // check hp name created
        $hp_name = $this->_student_check_student_hp_name($student);

        // check hp name exist in hp
        $this->_student_check_hp_name_exist_in_hp($hp_name);

        // check hp nam exist in db
        $this->_student_check_hp_name_exist_in_db($hp_name);

        // check key hp
        $key_hp = $this->_student_check_hp_key_for_hp_name($hp_name);

        // check key hp in db

        // check ical
        if( ! $is_fake )
            $this->_student_check_ical_for_hp_name($key_hp);
    }


    public function student_get_results($infos, $debugs, $fake = false)
    {
        $valids = [];
        $errors = [];

        foreach( $infos as $key => $info)
        {
            $info['debug'] = $debugs[$key];

            $is_valid = $this->student_is_valid($info, $fake);
            if ($is_valid)
            {
                if( $fake )
                {
                    $valids[] = $info;
                }
                else if( $info['has_hp_ical']['is_valid'] )
                {
                    $valids[] = $info;
                }
                else
                {
                    $errors[] = $info;
                }
            }
            else
            {
                $errors[] = $info;
            }
        }

        $result['valids'] = $valids;
        $result['errors'] = $errors;

        return $result;
    }


    public function student_get_students($numproeco = false)
    {
        if( $numproeco )
        {
            $students = StudentModel::where('actif', '1')
                ->where('numproeco', $numproeco)
                ->get();
        }
        else
        {
            $students = StudentModel::where('actif', '1')
                ->get();
        }

        return $students;
    }
}
