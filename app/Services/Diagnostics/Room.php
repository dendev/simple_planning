<?php

namespace App\Services\Diagnostics;

use App\Models\ConversionRoom;
use App\Models\Room as RoomModel;
use Illuminate\Support\Facades\DB;

trait Room
{
    private $_room_info;
    private $_room_debug;

    public function room_get_info()
    {
        return $this->_room_info;
    }

    public function room_get_debug()
    {
        return $this->_room_debug;
    }

    public function room_get_rooms( $name = false )
    {
        $name = str_replace("'", '', $name);

        if( $name )
        {
            $rooms = RoomModel::where('room_name', $name)->get();
        }
        else
        {
            $rooms = RoomModel::all();
        }

        return $rooms;
    }

    public function room_init_debug_infos($room = false)
    {
        $this->_room_debug['key_hyperplanning'] =  $room ? $room->key_hyperplanning : null;
        $this->_room_debug['room_name'] = $room ? $room->room_name : null;
        $this->_room_debug['implantation_name'] = $room ? $room->implantation_name : null;
    }

    public function room_run_diagnostic($room, $is_fake = false)
    {
        // initialise debug infos
        $this->room_init_debug_infos($room, $this->_room_debug);

        // hp_name == room->room_name
        $hp_name = $room->room_name;

        // check hp name exist in hp
        $this->_room_check_hp_name_exist_in_hp($hp_name);

        // check hp name exist in db
        $this->_room_check_hp_name_exist_in_db($hp_name);

        // check key hp
        $key_hp = $this->_room_check_hp_key_for_hp_name($hp_name);

        // check key hp in db

        // check ical
        if( ! $is_fake )
            $this->_room_check_ical_for_hp_name($key_hp);
    }


    public function room_check_rooms_exist($rooms)
    {
        $this->_room_info['exist']['label'] = "Vérifie le(s) rooms dans proeco";

        $this->_promotion_exist['exist'] = count($rooms);

        if ( count($rooms) > 0)
        {
            $this->_room_info['exist']['is_valid'] = true;
            $this->_room_info['exist']['msg'] = "Room(s) existe bien dans proecohenallux et est actif";
        }
        else
        {
            $this->_room_info['exist']['is_valid'] = false;
            $this->_room_info['exist']['msg'] = "Room(s) n'existe pas dans proecohenallux";
        }
    }

    private function _room_check_hp_name_exist_in_hp($hp_name)
    {
        $is_valid = false;

        $this->_room_debug['hp_name'] = $hp_name;

        if( in_array($hp_name, $this->_ref_room_names ) )
            $is_valid = true;

        $this->_room_info['has_hp_name_in_hp']['label'] = "Vérifie le nom hyperplanning de la room dans HP";

        if( $is_valid)
        {
            $this->_room_info['has_hp_name_in_hp']['is_valid'] = true;
            $this->_room_info['has_hp_name_in_hp']['msg'] = "Le nom de room existe bien dans hp";
        }
        else
        {
            $this->_room_info['has_hp_name_in_hp']['is_valid'] = false;
            $this->_room_info['has_hp_name_in_hp']['msg'] = "Le nom de room n'existe pas dans hp";
        }

        return $is_valid;
    }

    private function _room_check_hp_name_exist_in_db($hp_name)
    {
        $is_valid = false;

        $this->_room_debug['hp_name'] = $hp_name;

        $conversion_room = ConversionRoom::where('room_name', $hp_name)->first();
        if( $hp_name && $conversion_room)
            $is_valid = true;

        $this->_room_info['has_hp_name_in_db']['label'] = "Vérifie le nom hyperplanning de la room dans la DB de conversion";

        if( $is_valid)
        {
            $this->_room_info['has_hp_name_in_db']['is_valid'] = true;
            $this->_room_info['has_hp_name_in_db']['msg'] = "Le nom de room existe dans la table conversion_room";
        }
        else
        {
            $this->_room_info['has_hp_name_in_db']['is_valid'] = false;
            $this->_room_info['has_hp_name_in_db']['msg'] = "Le nom de room n'existe pas dans la table conversion_room";
        }

        return $is_valid;
    }

    private function _room_check_hp_key_for_hp_name($hp_name)
    {
        $is_valid = false;
        $key = false;

        $this->_room_debug['hp_name'] = $hp_name;

        $index =  array_search( $hp_name, $this->_ref_room_names );
        if( $index !== false )
        {
            $key = $this->_ref_room_keys[$index];
            if( $key )
            {
                $is_valid = true;
            }
        }

        $this->_room_info['has_hp_key']['label'] = "Vérifie que la clé de room existe dans HP";

        if( $is_valid)
        {
            $this->_room_info['has_hp_key']['is_valid'] = true;
            $this->_room_info['has_hp_key']['msg'] = "La clé de room existe bien dans hp";
        }
        else
        {
            $this->_room_info['has_hp_key']['is_valid'] = false;
            $this->_room_info['has_hp_key']['msg'] = "La clé de room n'existe pas dans hp";
        }

        return $key;
    }

    private function _room_check_ical_for_hp_name($key_hp)
    {
        $is_valid = false;

        $this->_room_debug['key_hp'] = $key_hp;
        $this->_room_debug['nb_weeks'] = 53;

        $ical = \PlanningManager::icalSalle($key_hp, 53);
        if( $ical )
            $is_valid = true;

        $this->_room_info['has_hp_ical']['label'] = "Vérifie que l'horaire existe dans HP";

        if( $is_valid )
        {
            $this->_room_info['has_hp_ical']['is_valid'] = true;
            $this->_room_info['has_hp_ical']['msg'] = "La room a bien un ical qui existe dans hp";
        }
        else
        {
            $this->_room_info['has_hp_ical']['is_valid'] = false;
            $this->_room_info['has_hp_ical']['msg'] = "La room n'a pas d'ical qui existe dans hp";
        }

        return $ical;
    }

    public function room_is_valid( $info, $fake = false)
    {
        if (
            $info['exist']['is_valid'] &&
            $info['has_hp_name_in_hp']['is_valid'] &&
            $info['has_hp_name_in_db']['is_valid'] &&
            $info['has_hp_key']['is_valid']
        )
        {
            if( $fake )
                $is_valid = true;
            else if( $this->_room_info['has_hp_ical']['is_valid'] )
                $is_valid = true;
            else
                $is_valid = false;
        }
        else
        {
            $is_valid = false;
        }

        return $is_valid;
    }

    public function room_get_results($infos, $debugs, $fake = false) // FIXME debug
    {
        $valids = [];
        $errors = [];

        foreach( $infos as $key => $info)
        {
            $info['debug'] = $debugs[$key];

            $is_valid = $this->room_is_valid($info, $fake);
            if ($is_valid)
            {
                if( $fake )
                {
                    $valids[] = $info;
                }
                else if( $info['has_hp_ical']['is_valid'] )
                {
                    $valids[] = $info;
                }
                else
                {
                    $errors[] = $info;
                }
            }
            else
            {
                $errors[] = $info;
            }
        }

        $result['valids'] = $valids;
        $result['errors'] = $errors;

        return $result;
    }
}
