<?php

namespace App\Services\Diagnostics;

use App\Models\ConversionExterne;
use App\Models\ConversionTeacher;
use App\Models\Extern as ExternModel;

trait Extern
{
    private $_extern_info;
    private $_extern_debug = ['portail_magic_login' => 'https://portail.henallux.be/login?ignore'];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function extern_get_info()
    {
        return $this->_extern_info;
    }

    public function extern_get_debug()
    {
        return $this->_extern_debug;
    }

    public function extern_check_externs_exist($externs)
    {
        $this->_extern_info['exist']['label'] = "Vérifie l'externe est dans sheldon";

        if (!$externs->isEmpty())
        {
            $this->_extern_info['exist']['is_valid'] = true;
            $this->_extern_info['exist']['msg'] = "Externe existe bien dans sheldon";
        }
        else
        {
            $this->_extern_info['exist']['is_valid'] = false;
            $this->_extern_info['exist']['msg'] = "Externe n'existe pas dans sheldon";
        }
    }

    public function extern_init_debug_infos($extern = false, $debug = [])
    {
        $debug['login'] = $extern ? 'ext' . $extern->abreviation. '@henallux.be' : null;
        $debug['id_people'] = $extern ? $extern->id_people : null;

        $this->_extern_debug = $debug;
    }

    private function _extern_check_extern_hp_code($extern)
    {
        $hp_code = $extern->get_hp_code();

        $this->_extern_debug['hp_code'] = $hp_code;

        $this->_extern_info['has_hp_code']['label'] = "Vérifie le nom hyperplanning, issut de sheldon, pour l'externe";

        if( $hp_code )
        {
            $this->_extern_info['has_hp_code']['is_valid'] = true;
            $this->_extern_info['has_hp_code']['msg'] = "L'extern à un nom hp crée depuis sheldon";
        }
        else
        {
            $this->_extern_info['has_hp_code']['is_valid'] = false;
            $this->_extern_info['has_hp_code']['msg'] = "Impossible de crée le nom hp de l'externe depuis sheldon";
        }

        return $hp_code;
    }

    private function _extern_check_hp_code_exist_in_hp($hp_code)
    {
        $is_valid = false;

        $this->_extern_debug['hp_code'] = $hp_code;

        if( in_array($hp_code, $this->_ref_extern_codes) )
            $is_valid = true;

        $this->_extern_info['has_hp_code_in_hp']['label'] = "Vérifie le nom hyperplanning de l'externe dans HP";

        if( $is_valid)
        {
            $this->_extern_info['has_hp_code_in_hp']['is_valid'] = true;
            $this->_extern_info['has_hp_code_in_hp']['msg'] = "L'externe à un nom qui existe dans hp";
        }
        else
        {
            $this->_extern_info['has_hp_code_in_hp']['is_valid'] = false;
            $this->_extern_info['has_hp_code_in_hp']['msg'] = "L'externe n'a pas de nom qui existe dans hp";
        }

        return $is_valid;
    }

    private function _extern_check_hp_code_exist_in_db($hp_code)
    {
        $is_valid = false;

        $this->_extern_debug['hp_code'] = $hp_code;

        $conversion_extern = ConversionExterne::where('code_hyperplanning', $hp_code)->first();
        if( $hp_code && $conversion_extern)
            $is_valid = true;

        $this->_extern_info['has_hp_code_in_db']['label'] = "Vérifie le nom hyperplanning du mdp existe dans la DB de conversion";

        if( $is_valid)
        {
            $this->_extern_info['has_hp_code_in_db']['is_valid'] = true;
            $this->_extern_info['has_hp_code_in_db']['msg'] = "Le mdp à un nom qui existe dans la table conversion_extern";
        }
        else
        {
            $this->_extern_info['has_hp_code_in_db']['is_valid'] = false;
            $this->_extern_info['has_hp_code_in_db']['msg'] = "Le mdp n'a pas de nom qui existe dans la table conversion_extern";
        }

        return $is_valid;
    }

    private function _extern_check_hp_key_for_hp_code($hp_code )
    {
        $is_valid = false;
        $key = false;

        $this->_extern_debug['hp_code'] = $hp_code;

        $index =  array_search( $hp_code, $this->_ref_extern_codes);
        if( $index !== false )
        {
            $key = $this->_ref_extern_keys[$index];
            if( $key )
            {
                $is_valid = true;
            }
        }

        $this->_extern_info['has_hp_key']['label'] = "Vérifie que le code mdp existe dans HP";

        if( $is_valid)
        {
            $this->_extern_info['has_hp_key']['is_valid'] = true;
            $this->_extern_info['has_hp_key']['msg'] = "Le mdp a une clé qui existe dans hp";
        }
        else
        {
            $this->_extern_info['has_hp_key']['is_valid'] = false;
            $this->_extern_info['has_hp_key']['msg'] = "Le mdp n'a pas de clé qui existe dans hp";
        }


        return $key;
    }

    private function _extern_check_ical_for_hp_code($key_hp)
    {
        $is_valid = false;

        $this->_extern_debug['key_hp'] = $key_hp;
        $this->_extern_debug['nb_weeks'] = 53;

        $ical = \PlanningManager::icalPromotion($key_hp, 53);
        if( $ical )
            $is_valid = true;

        $this->_extern_info['has_hp_ical']['label'] = "Vérifie que l'horaire existe dans HP";

        if( $is_valid )
        {
            $this->_extern_info['has_hp_ical']['is_valid'] = true;
            $this->_extern_info['has_hp_ical']['msg'] = "Le mdp a un ical qui existe dans hp";
        }
        else
        {
            $this->_extern_info['has_hp_ical']['is_valid'] = false;
            $this->_extern_info['has_hp_ical']['msg'] = "Le mdp n'a pas d'ical existe dans hp";
        }

        return $ical;
    }

    public function extern_is_valid($info, $fake = false)
    {
        if (
            $info['exist']['is_valid'] &&
            $info['has_hp_code']['is_valid'] &&
            $info['has_hp_code_in_hp']['is_valid'] &&
            $info['has_hp_code_in_db']['is_valid'] &&
            $info['has_hp_key']['is_valid']
        )
        {
            if( $fake )
                $is_valid = true;
            else if( $this->_extern_info['has_hp_ical']['is_valid'] )
                $is_valid = true;
            else
                $is_valid = false;
        }
        else
        {
            $is_valid = false;
        }

        return $is_valid;
    }

    public function extern_run_diagnostic($extern, $is_fake = false)
    {
        // initialise debug infos
        $this->extern_init_debug_infos($extern, $this->_extern_debug);

        // check hp name created
        $hp_code = $this->_extern_check_extern_hp_code($extern);

        // check hp name exist in hp
        $this->_extern_check_hp_code_exist_in_hp($hp_code);

        // check hp nam exist in db
        $this->_extern_check_hp_code_exist_in_db($hp_code);

        // check key hp
        $key_hp = $this->_extern_check_hp_key_for_hp_code($hp_code);

        // check key hp in db

        // check ical
        if( ! $is_fake )
            $this->_extern_check_ical_for_hp_code($key_hp);
    }


    public function extern_get_results($infos, $debugs, $fake = false)
    {
        $valids = [];
        $errors = [];

        foreach( $infos as $key => $info)
        {
            $info['debug'] = $debugs[$key];

            $is_valid = $this->extern_is_valid($info, $fake);
            if ($is_valid)
            {
                if( $fake )
                {
                    $valids[] = $info;
                }
                else if( $info['has_hp_ical']['is_valid'] )
                {
                    $valids[] = $info;
                }
                else
                {
                    $errors[] = $info;
                }
            }
            else
            {
                $errors[] = $info;
            }
        }

        $result['valids'] = $valids;
        $result['errors'] = $errors;

        return $result;
    }


    public function extern_get_externs($login = false)
    {
        if( $login )
        {
            $abbreviation = str_replace('ext', '', $login);
            $externs = ExternModel::where('abreviation', $abbreviation)
                ->get();
        }
        else
        {
            $externs = ExternModel::all();
        }

        return $externs;
    }
}
