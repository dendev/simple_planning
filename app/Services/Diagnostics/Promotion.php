<?php

namespace App\Services\Diagnostics;

use App\Models\ConversionPromotion;
use Illuminate\Support\Facades\DB;

trait Promotion
{
    private $_promotion_info;
    private $_promotion_debug;

    public function promotion_get_info()
    {
        return $this->_promotion_info;
    }

    public function promotion_get_debug()
    {
        return $this->_promotion_debug;
    }

    public function promotion_get_promotions($name = false)
    {

        if( $name )
        {
            $promotions = DB::connection('proecohenallux')->select("
                SELECT cp.idClasse AS promotion_id, cp.intitule AS promotion_label, im.num as implantation_id, im.nomimpl AS implantation_label, oi.id AS orientation_id, CONCAT_WS( ' - ', oi.code, oi.intitule, oi.bib ) AS orientation_label, cp.annee, cp.classe
                FROM proecohenallux.classesproeco AS cp
                INNER JOIN orientations AS oi ON oi.id = cp.fkOrientation
                INNER JOIN implantation AS im ON im.num = cp.fkImplantation
                WHERE cp.intitule = ?
                ", [$name]);
        }
        else
        {
            $promotions = DB::connection('proecohenallux')->select("
                SELECT cp.idClasse AS promotion_id, cp.intitule AS promotion_label, im.num as implantation_id, im.nomimpl AS implantation_label, oi.id AS orientation_id, CONCAT_WS( ' - ', oi.code, oi.intitule, oi.bib ) AS orientation_label, cp.annee, cp.classe
                FROM proecohenallux.classesproeco AS cp
                INNER JOIN orientations AS oi ON oi.id = cp.fkOrientation
                INNER JOIN implantation AS im ON im.num = cp.fkImplantation
                ");
        }

        return $promotions;
    }

    public function promotion_init_debug_infos($promotion = false)
    {
        $this->_promotion_debug['classesproeco_id'] = $promotion ? $promotion->promotion_id : null;
        $this->_promotion_debug['implantation_id'] = $promotion ? $promotion->implantation_id : null;
        $this->_promotion_debug['orientation_id'] = $promotion ? $promotion->orientation_id : null;
        $this->_promotion_debug['classesproeco_classe'] = $promotion ? $promotion->classe : null;
        $this->_promotion_debug['classesproeco_annee'] = $promotion ? $promotion->annee : null;
    }

    public function promotion_run_diagnostic($promotion, $is_fake = false)
    {
        // initialise debug infos
        $this->promotion_init_debug_infos($promotion, $this->_promotion_debug);

        // check hp name created
        $hp_name = $this->_promotion_check_promotion_hp_name($promotion);

        // check hp name exist in hp
        $this->_promotion_check_hp_name_exist_in_hp($hp_name);

        // check hp name exist in db
        $this->_promotion_check_hp_name_exist_in_db($hp_name);

        // check key hp
        $key_hp = $this->_promotion_check_hp_key_for_hp_name($hp_name);

        // check key hp in db

        // check ical
        if( ! $is_fake )
            $this->_promotion_check_ical_for_hp_name($key_hp);
    }


    public function promotion_check_promotions_exist($promotions)
    {
        $this->_promotion_info['exist']['label'] = "Vérifie le(s) promotions dans proeco";

        if ( count($promotions) > 0)
        {
            $this->_promotion_info['exist']['is_valid'] = true;
            $this->_promotion_info['exist']['msg'] = "Promotion(s) existe bien dans proecohenallux et est actif";
        }
        else
        {
            $this->_promotion_info['exist']['is_valid'] = false;
            $this->_promotion_info['exist']['msg'] = "Promotion(s) n'existe pas dans proecohenallux";
        }

    }

    private function _promotion_check_promotion_hp_name($promotion)
    {
        $hp_name = $promotion->promotion_label;

        $this->_promotion_debug['hp_name'] = $hp_name;

        $this->_promotion_info['has_hp_name']['label'] = "Vérifie le nom hyperplanning, issut de proeco, de la promotion ";

        if( $hp_name )
        {
            $this->_promotion_info['has_hp_name']['is_valid'] = true;
            $this->_promotion_info['has_hp_name']['msg'] = "La promotion a un nom d'orientation hp crée depuis proeco";
        }
        else
        {
            $this->_promotion_info['has_hp_name']['is_valid'] = false;
            $this->_promotion_info['has_hp_name']['msg'] = "Impossible de crée le nom d'orientation hp de la promotion depuis proeco";
        }

        return $hp_name;
    }

    private function _promotion_check_hp_name_exist_in_hp($hp_name)
    {
        $is_valid = false;

        $this->_promotion_debug['hp_name'] = $hp_name;

        if( in_array($hp_name, $this->_ref_promotion_names ) )
            $is_valid = true;

        $this->_promotion_info['has_hp_name_in_hp']['label'] = "Vérifie le nom hyperplanning de la promotion dans HP";

        if( $is_valid)
        {
            $this->_promotion_info['has_hp_name_in_hp']['is_valid'] = true;
            $this->_promotion_info['has_hp_name_in_hp']['msg'] = "Le nom de promotion existe bien dans hp";
        }
        else
        {
            $this->_promotion_info['has_hp_name_in_hp']['is_valid'] = false;
            $this->_promotion_info['has_hp_name_in_hp']['msg'] = "Le nom de promotion n'existe pas dans hp";
        }

        return $is_valid;
    }

    private function _promotion_check_hp_name_exist_in_db($hp_name)
    {
        $is_valid = false;

        $this->_promotion_debug['hp_name'] = $hp_name;

        $conversion_promotion = ConversionPromotion::where('name_hyperplanning', $hp_name)->first();
        if( $hp_name && $conversion_promotion)
            $is_valid = true;

        $this->_promotion_info['has_hp_name_in_db']['label'] = "Vérifie le nom hyperplanning de la promotion dans la DB de conversion";

        if( $is_valid)
        {
            $this->_promotion_info['has_hp_name_in_db']['is_valid'] = true;
            $this->_promotion_info['has_hp_name_in_db']['msg'] = "Le nom de promotion existe dans la table conversion_promotion";
        }
        else
        {
            $this->_promotion_info['has_hp_name_in_db']['is_valid'] = false;
            $this->_promotion_info['has_hp_name_in_db']['msg'] = "Le nom de promotion n'existe pas dans la table conversion_promotion";
        }

        return $is_valid;
    }

    private function _promotion_check_hp_key_for_hp_name($hp_name)
    {
        $is_valid = false;
        $key = false;

        $this->_promotion_debug['hp_name'] = $hp_name;

        $index =  array_search( $hp_name, $this->_ref_promotion_names );
        if( $index !== false )
        {
            $key = $this->_ref_promotion_keys[$index];
            if( $key )
            {
                $is_valid = true;
            }
        }

        $this->_promotion_info['has_hp_key']['label'] = "Vérifie que la clé de promotion existe dans HP";

        if( $is_valid)
        {
            $this->_promotion_info['has_hp_key']['is_valid'] = true;
            $this->_promotion_info['has_hp_key']['msg'] = "La clé de promotion existe bien dans hp";
        }
        else
        {
            $this->_promotion_info['has_hp_key']['is_valid'] = false;
            $this->_promotion_info['has_hp_key']['msg'] = "La clé de promotion n'existe pas dans hp";
        }

        return $key;
    }

    private function _promotion_check_ical_for_hp_name($key_hp)
    {
        $is_valid = false;

        $this->_promotion_debug['key_hp'] = $key_hp;
        $this->_promotion_debug['nb_weeks'] = 53;

        $ical = \PlanningManager::icalPromotion($key_hp, 53);
        if( $ical )
            $is_valid = true;

        $this->_promotion_info['has_hp_ical']['label'] = "Vérifie que l'horaire existe dans HP";

        if( $is_valid )
        {
            $this->_promotion_info['has_hp_ical']['is_valid'] = true;
            $this->_promotion_info['has_hp_ical']['msg'] = "La promotion a bien un ical qui existe dans hp";
        }
        else
        {
            $this->_promotion_info['has_hp_ical']['is_valid'] = false;
            $this->_promotion_info['has_hp_ical']['msg'] = "La promotion n'a pas d'ical qui existe dans hp";
        }

        return $ical;
    }

    public function promotion_is_valid( $info, $fake = false)
    {
        if (
            $info['exist']['is_valid'] &&
            $info['has_hp_name']['is_valid'] &&
            $info['has_hp_name_in_hp']['is_valid'] &&
            $info['has_hp_name_in_db']['is_valid'] &&
            $info['has_hp_key']['is_valid']
        )
        {
            if( $fake )
                $is_valid = true;
            else if( $this->_promotion_info['has_hp_ical']['is_valid'] )
                $is_valid = true;
            else
                $is_valid = false;
        }
        else
        {
            $is_valid = false;
        }

        return $is_valid;
    }

    public function promotion_get_results($infos, $debugs, $fake = false) // FIXME debug
    {
        $valids = [];
        $errors = [];

        foreach( $infos as $key => $info)
        {
            $info['debug'] = $debugs[$key];

            $is_valid = $this->promotion_is_valid($info, $fake);
            if ($is_valid)
            {
                if( $fake )
                {
                    $valids[] = $info;
                }
                else if( $info['has_hp_ical']['is_valid'] )
                {
                    $valids[] = $info;
                }
                else
                {
                    $errors[] = $info;
                }
            }
            else
            {
                $errors[] = $info;
            }
        }

        $result['valids'] = $valids;
        $result['errors'] = $errors;

        return $result;
    }
}
