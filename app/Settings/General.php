<?php

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class General extends Settings
{
    public int $october_day;
    public int $march_day;

    public static function group(): string
    {
        return 'general';
    }
}
