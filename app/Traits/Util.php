<?php

namespace App\Traits;

//use App\Models\Setting;
use App\Libraries\IcalFileParser;
use Carbon\Carbon;

trait Util
{
    private function _fix_winter_hour($ical)
    {
        $rows = explode("\n", $ical);
        foreach( $rows as &$row )
        {
            if( ( strpos($row, 'DTSTART' ) !== false ) || ( strpos($row, 'DTEND' ) !== false ) )
            {
                // extract
                $re = '/(?<=DTSTART:|DTEND:)(.*)(?=Z)/m';
                $matches = [];
                preg_match($re, $row, $matches);
                if( count( $matches ) > 0 )
                {
                    $match = $matches[0];

                    // transform in date
                    $year = substr($match, 0, 4);
                    $month = substr($match, 4, 2);
                    $day = substr($match, 6, 2);

                    $hour = substr($match, 9, 2);
                    $minute = substr($match, 11, 2);
                    $segond = substr($match, 13, 2);
                    $current = Carbon::create($year, $month, $day, $hour, $minute, $segond);

                    // fix limit
                    $now = Carbon::now();
                    //$october_day = Setting::get('october_day_for_winter_hour');
                    $october_day = 30;

                    // octobre -> decembre
                    $begin_first_previous_year = Carbon::create($now->year - 1, 11, 8, 03, 00, 00);
                    $end_first_previous_year   = Carbon::create($now->year - 1, 12, 31, 23, 59, 59);
                    $begin_first_current_year  = Carbon::create($now->year        , 11, 8, 03, 00, 00);
                    $end_first_current_year    = Carbon::create($now->year        , 12, 31, 23, 59, 59);
                    $begin_first_next_year     = Carbon::create($now->year + 1, 11, 8, 03, 00, 00);
                    $end_first_next_year       = Carbon::create($now->year + 1, 12, 31, 23, 59, 59);

                    // janvier -> mars
                    $begin_second_previous_year = Carbon::create($now->year - 1, 1, 1, 00, 00, 01);
                    $end_second_previous_year   = Carbon::create($now->year - 1, 3, 28, 02, 00, 00);
                    $begin_second_current_year  = Carbon::create($now->year        , 1, 1, 00, 00, 01);
                    $end_second_current_year    = Carbon::create($now->year        , 3, 28, 02, 00, 00);
                    $begin_second_next_year     = Carbon::create($now->year + 1, 1, 1, 00, 00, 01);
                    $end_second_next_year       = Carbon::create($now->year + 1, 3, 28, 02, 00, 00);

                    // check if event is in winter time
                    if (
                        ( $current->gte($begin_first_previous_year)  && $current->lte($end_first_previous_year) )  ||
                        ( $current->gte($begin_second_previous_year) && $current->lte($end_second_previous_year) ) ||
                        ( $current->gte($begin_first_current_year)   && $current->lte($end_first_current_year) )   ||
                        ( $current->gte($begin_second_current_year)  && $current->lte($end_second_current_year) )  ||
                        ( $current->gte($begin_first_next_year)      && $current->lte($end_first_next_year) )      ||
                        ( $current->gte($begin_second_next_year)     && $current->lte($end_second_next_year) )
                    )
                    {
                        $current->subHour();
                        // convert to good format and fix diff hour
                        $string_date = $current->format('Ymd\\THis');

                        //preg_replace("/$match/", $string_date, $ical, 1);
                        $row = str_replace($match, $string_date, $row);
                    }
                }
            }
        }
        return implode( "\n", $rows );
    }

    private function _fix_clean_ical($ical)
    {
	    // Remove Z for import o365 && google
	    $re = '/DTSTART:.*?(Z)/m';
	    $matches = [];
	    preg_match_all($re, $ical, $matches);
	    foreach( $matches[0] as $match )
	    {
		    $tmp = str_replace('Z', '', $match);
		    $formated_value = str_replace('DTSTART', 'DTSTART;TZID=Europe/Brussels', $tmp);
		    $ical = str_replace($match, $formated_value, $ical);
	    }

	    $re = '/DTEND:.*?(Z)/m';
	    $matches = [];
	    preg_match_all($re, $ical, $matches);
	    foreach( $matches[0] as $match )
	    {
		    $tmp = str_replace('Z', '', $match);
		    $formated_value = str_replace('DTEND', 'DTEND;TZID=Europe/Brussels', $tmp);
		    $ical = str_replace($match, $formated_value, $ical);
	    }

	    return $ical;
    }

    private function _convert_ical_to_array($ical, $calendar)
    {
        $cal = new IcalFileParser();
        $calendar = array_merge($calendar, $cal->parse($ical, 'array'));
        return $calendar;
    }

    private function _format_api_keys($keys)
    {
        $keys = str_replace("'", '"', $keys);
        $keys = json_decode($keys);
        $keys = is_array($keys) ? $keys : [$keys];

        return $keys;
    }

    /*
    private function _format_ical($ical, $format, $calendar = [])
    {
        $ical = $this->_format_ical($ical);

        if( $format === 'json' )
            $calendar = $this->_convert_ical_to_json($ical, $calendar);
        else
            $calendar = $this->_fix_clean_ical($ical);

        return $calendar;
    }
    */
}
