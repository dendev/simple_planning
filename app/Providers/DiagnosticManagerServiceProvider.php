<?php

namespace App\Providers;

use App\Services\DiagnosticManagerService;
use Illuminate\Support\ServiceProvider;

class DiagnosticManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'diagnostic_manager', function ($app) {
            return new DiagnosticManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
