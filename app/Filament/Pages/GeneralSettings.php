<?php

namespace App\Filament\Pages;

use App\Settings\General;
use Filament\Forms\Components\TextInput;
use Filament\Pages\SettingsPage;

class GeneralSettings extends SettingsPage
{
    protected static ?string $navigationIcon = 'heroicon-o-cog';

    protected static string $settings = General::class;

    protected function getFormSchema(): array
    {
        return [
            TextInput::make('october_day')
                ->label(__('admin/settings.october_day_label'))
                ->default(30)
                ->helperText(__('admin/settings.october_day_hint'))
                ->required(),
              TextInput::make('march_day')
                ->label(__('admin/settings.march_day_label'))
                ->default(30)
                ->helperText(__('admin/settings.march_day_hint'))
                ->required(),
        ];
    }
}
