<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ConversionRoomResource\Pages;
use App\Filament\Resources\ConversionRoomResource\RelationManagers;
use App\Models\ConversionRoom;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class ConversionRoomResource extends Resource
{
    protected static ?string $model = ConversionRoom::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('key_hyperplanning')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('room_name')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('implantation_prefix')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('implantation_name')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('implantation_id')
                    ->required()
                    ->maxLength(255),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id')->sortable(),
                Tables\Columns\TextColumn::make('key_hyperplanning'),
                Tables\Columns\TextColumn::make('room_name'),
                Tables\Columns\TextColumn::make('implantation_prefix'),
                Tables\Columns\TextColumn::make('implantation_name'),
                Tables\Columns\TextColumn::make('implantation_id'),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListConversionRooms::route('/'),
            'create' => Pages\CreateConversionRoom::route('/create'),
            'edit' => Pages\EditConversionRoom::route('/{record}/edit'),
        ];
    }
}
