<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ConversionExterneResource\Pages;
use App\Filament\Resources\ConversionExterneResource\RelationManagers;
use App\Models\ConversionExterne;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class ConversionExterneResource extends Resource
{
    protected static ?string $model = ConversionExterne::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('id_people')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('login')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('name')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('firstname')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('email')
                    ->email()
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('code_hyperplanning')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('key_hyperplanning')
                    ->required()
                    ->maxLength(255),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id')->sortable(),
                Tables\Columns\TextColumn::make('id_people'),
                Tables\Columns\TextColumn::make('login'),
                Tables\Columns\TextColumn::make('name'),
                Tables\Columns\TextColumn::make('firstname'),
                Tables\Columns\TextColumn::make('email'),
                Tables\Columns\TextColumn::make('code_hyperplanning'),
                Tables\Columns\TextColumn::make('key_hyperplanning'),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListConversionExternes::route('/'),
            'create' => Pages\CreateConversionExterne::route('/create'),
            'edit' => Pages\EditConversionExterne::route('/{record}/edit'),
        ];
    }
}
