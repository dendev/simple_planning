<?php

namespace App\Filament\Resources\ConversionRoomResource\Pages;

use App\Filament\Resources\ConversionRoomResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateConversionRoom extends CreateRecord
{
    protected static string $resource = ConversionRoomResource::class;
}
