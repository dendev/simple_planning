<?php

namespace App\Filament\Resources\ConversionRoomResource\Pages;

use App\Filament\Resources\ConversionRoomResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditConversionRoom extends EditRecord
{
    protected static string $resource = ConversionRoomResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
