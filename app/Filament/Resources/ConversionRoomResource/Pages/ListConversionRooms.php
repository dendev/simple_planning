<?php

namespace App\Filament\Resources\ConversionRoomResource\Pages;

use App\Filament\Resources\ConversionRoomResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListConversionRooms extends ListRecords
{
    protected static string $resource = ConversionRoomResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
