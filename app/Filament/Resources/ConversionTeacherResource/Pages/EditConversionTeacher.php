<?php

namespace App\Filament\Resources\ConversionTeacherResource\Pages;

use App\Filament\Resources\ConversionTeacherResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditConversionTeacher extends EditRecord
{
    protected static string $resource = ConversionTeacherResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
