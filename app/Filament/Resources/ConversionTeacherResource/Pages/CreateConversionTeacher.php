<?php

namespace App\Filament\Resources\ConversionTeacherResource\Pages;

use App\Filament\Resources\ConversionTeacherResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateConversionTeacher extends CreateRecord
{
    protected static string $resource = ConversionTeacherResource::class;
}
