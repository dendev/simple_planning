<?php

namespace App\Filament\Resources\ConversionPromotionResource\Pages;

use App\Filament\Resources\ConversionPromotionResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateConversionPromotion extends CreateRecord
{
    protected static string $resource = ConversionPromotionResource::class;
}
