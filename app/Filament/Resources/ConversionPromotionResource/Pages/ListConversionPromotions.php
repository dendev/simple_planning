<?php

namespace App\Filament\Resources\ConversionPromotionResource\Pages;

use App\Filament\Resources\ConversionPromotionResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListConversionPromotions extends ListRecords
{
    protected static string $resource = ConversionPromotionResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
