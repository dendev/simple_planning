<?php

namespace App\Filament\Resources\ConversionExterneResource\Pages;

use App\Filament\Resources\ConversionExterneResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateConversionExterne extends CreateRecord
{
    protected static string $resource = ConversionExterneResource::class;
}
