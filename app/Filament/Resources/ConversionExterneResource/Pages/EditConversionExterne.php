<?php

namespace App\Filament\Resources\ConversionExterneResource\Pages;

use App\Filament\Resources\ConversionExterneResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditConversionExterne extends EditRecord
{
    protected static string $resource = ConversionExterneResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
