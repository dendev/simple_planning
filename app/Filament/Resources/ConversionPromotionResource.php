<?php

namespace App\Filament\Resources;

use AlperenErsoy\FilamentExport\Actions\FilamentExportBulkAction;
use App\Filament\Resources\ConversionPromotionResource\Pages;
use App\Filament\Resources\ConversionPromotionResource\RelationManagers;
use App\Models\ConversionPromotion;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Filters\Filter;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class ConversionPromotionResource extends Resource
{
    protected static ?string $model = ConversionPromotion::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('promotion_id')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('promotion_label')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('implantation_id')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('implantation_label')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('orientation_id')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('orientation_label')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('annee')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('classe')
                    ->maxLength(255),
                Forms\Components\TextInput::make('name_hyperplanning')
                    ->maxLength(255),
                Forms\Components\TextInput::make('key_hyperplanning')
                    ->maxLength(255),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id')->sortable()->searchable(),
                Tables\Columns\TextColumn::make('promotion_id')->sortable(),
                Tables\Columns\TextColumn::make('promotion_label')->sortable(),
                Tables\Columns\TextColumn::make('implantation_id')->sortable(),
                Tables\Columns\TextColumn::make('implantation_label')->sortable(),
                Tables\Columns\TextColumn::make('orientation_id')->sortable(),
                Tables\Columns\TextColumn::make('orientation_label')->sortable(),
                Tables\Columns\TextColumn::make('annee')->sortable(),
                Tables\Columns\TextColumn::make('classe')->sortable(),
                Tables\Columns\TextColumn::make('name_hyperplanning')->sortable(),
                Tables\Columns\TextColumn::make('key_hyperplanning')->sortable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime(),
            ])
            ->filters([
                Filter::make('is_empty_hyperplanning_code')
                    ->query(fn (Builder $query): Builder => $query->whereNull('name_hyperplanning'))
                    ->name(__('admin/promotions.filter_is_empty_hyperplanning_code_label')),

                SelectFilter::make('implantation_id')
                    ->options([
                        1 => __('admin/promotions.filter_is_implantation_ie_label'),
                        2 => __('admin/promotions.filter_is_implantation_pa_label'),
                        3 => __('admin/promotions.filter_is_implantation_ma_label'),
                        4 => __('admin/promotions.filter_is_implantation_ch_label'),
                        5 => __('admin/promotions.filter_is_implantation_so_label'),
                        6 => __('admin/promotions.filter_is_implantation_cn_label'),
                        7 => __('admin/promotions.filter_is_implantation_pb_label'),
                        8 => __('admin/promotions.filter_is_implantation_cc_label'),
                        9 => __('admin/promotions.filter_is_implantation_ea_label'),
                        10 => __('admin/promotions.filter_is_implantation_es_label'),
                        11 => __('admin/promotions.filter_is_implantation_iv_label'),
                        12 => __('admin/promotions.filter_is_implantation_sc_label'),
                        13 => __('admin/promotions.filter_is_implantation_mi_label'),
                    ])
                    ->name(__('admin/promotions.filter_is_implantation_label')),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
                FilamentExportBulkAction::make('export'),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListConversionPromotions::route('/'),
            'create' => Pages\CreateConversionPromotion::route('/create'),
            'edit' => Pages\EditConversionPromotion::route('/{record}/edit'),
        ];
    }
}
