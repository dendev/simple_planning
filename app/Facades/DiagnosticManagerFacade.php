<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class DiagnosticManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'diagnostic_manager';
    }
}
