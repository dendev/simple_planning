<?php

namespace App\Console\Commands;

use App\MdpHyperplanning;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Cast\Object_;

class UpdateConversions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple_planning:update_conversions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run all update conversion cmd';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('simple_planning:update_conversion_teacher');
        $this->call('simple_planning:update_conversion_externe');
        $this->call('simple_planning:update_conversion_room');
        $this->call('simple_planning:update_conversion_promotion');
    }
}
