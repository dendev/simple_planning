<?php

namespace App\Console\Commands;

use App\Console\Commands\Diagnostics\Util;
use Illuminate\Console\Command;

class DiagnosticPromotion extends Command
{
    use Util;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple_planning:diagnostic_promotion {name? : promotion name LIKE IE-IG-1B-D} {--f|fake : dont get icla}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Check if 'promotion' proecohenallux exist in HP";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // refs from hp
        \DiagnosticManager::init_promotion_refs();

        // is fake
        $is_fake = $this->option('fake') === true;

        // is single
        $is_single = ! empty($this->argument('name')) ;

        // basic
        $infos = [];
        $debugs = [];

        // get and check
        $name = $this->argument('name');
        $promotions = \DiagnosticManager::promotion_get_promotions($name);
        \DiagnosticManager::promotion_check_promotions_exist($promotions);

        // each promotion
        foreach( $promotions as $index => $promotion )
        {
            $this->info("\n* Travail sur promotion {$promotion->promotion_label}");

            // diagnostic
            \DiagnosticManager::promotion_run_diagnostic($promotion, $is_fake);

            // get current info && debug
            $info = \DiagnosticManager::promotion_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::promotion_get_debug();

            // quick info
            $this->_brief(\DiagnosticManager::promotion_is_valid($info, $is_fake ) );
        }

        // if no promotions
        if( count( $promotions ) == 0 )
        {
            \DiagnosticManager::promotion_init_debug_infos();
            $info = \DiagnosticManager::promotion_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::promotion_get_debug();
        }

        // conclusion
        $this->info("\n** Conclusion");
        $results = \DiagnosticManager::promotion_get_results($infos, $debugs, $is_fake);
        $this->_output_results($results, $is_single);

        // remark ( help )
        $this->info("\n* Remarque");
        $this->_remark();
    }

    private function _remark()
    {
        $this->info("** L'affichage de l'horaire portail dépend du nombr d'heure à afficher par le portail.");
        $this->info("** Le lien entre proeco et hyperplanning se fait via la création de tables de conversions exécuté toutes les 24h");
        $this->info("\n");
    }
}
