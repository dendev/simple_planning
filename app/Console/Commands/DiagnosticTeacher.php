<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\Commands\Diagnostics\Util;

class DiagnosticTeacher extends Command
{
    use Util;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple_planning:diagnostic_teacher {login? : mdp login} {--f|fake : no ical}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if teacher/mdp has a planning';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // refs from hp
        \DiagnosticManager::init_teacher_refs();

        // is fake
        $is_fake = $this->option('fake') === true;

        // is single
        $is_single = ! empty($this->argument('login')) ;

        // basic
        $infos = [];
        $debugs = [];

        // get and check
        $login = $this->argument('login');
        $teachers = \DiagnosticManager::teacher_get_teachers($login);
        \DiagnosticManager::teacher_check_teachers_exist($teachers);

        // each teacher
        foreach( $teachers as $index => $teacher )
        {
            $this->info("\n* Travail sur mdp login {$teacher->login}");

            // diagnostic
            \DiagnosticManager::teacher_run_diagnostic($teacher, $is_fake);

            // get current info && debug
            $info = \DiagnosticManager::teacher_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::teacher_get_debug();

            // quick info
            $this->_brief(\DiagnosticManager::teacher_is_valid( $info, $is_fake ) );
        }

        // if no teacher
        if( count( $teachers ) == 0 )
        {
            \DiagnosticManager::teacher_init_debug_infos();
            $info = \DiagnosticManager::teacher_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::teacher_get_debug();
        }

        // conclusion
        $this->info("\n** Conclusion");
        $results = \DiagnosticManager::teacher_get_results($infos, $debugs, $is_fake);
        $this->_output_results($results, $is_single);

        // remark ( help )
        $this->info("\n* Remarque");
        $this->_remark();
    }

    private function _remark()
    {
        $this->info("** L'affichage de l'horaire portail dépend du nombr d'heure à afficher par le portail.");
        $this->info("** Le lien entre proeco et hyperplanning se fait via la création de tables de conversions exécuté toutes les 24h");
        $this->info("\n");
    }
}
