<?php

namespace App\Console\Commands;

use App\Models\ConversionRoom;
use App\Models\Local;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateConversionRoom extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple_planning:update_conversion_room';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate conversion_rooms with locals datas from hyperplanning';

    /**
     * The client for hyperplanning service
     *
     * @var Object SoapClient
     */
    protected $client;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $rooms = \PlanningManager::toutesLesSalles();
        $names = \PlanningManager::nomsTableauDeSalles($rooms);

        // waiting sheldon
        $implantations_name_ref = [
            'IE' => 'Iesn',
            'PA' => 'Paramédical',
            'MA' => 'Malonne',
            'CH' => 'Champion',
            'SO' => 'Social Namur',
            'CN' => 'Mias', // == Cardijn Namur
            'PB' => 'Bastogne',
            'CC' => 'Arlon',
            'ES' => 'Seraing',
            'IV' => 'Virton',
            'SC' => 'Siège Central',
            'MI' => 'Masi'
        ];

        $implantations_id_ref = [
            'IE' => 1,
            'PA' => 2,
            'MA' => 3,
            'CH' => 4,
            'SO' => 5,
            'CN' => 6, // ????
            'PB' => 7,
            'CC' => 8,
            'ES' => 10,
            'IV' => 11,
            'SC' => 12,
            'MI' => 13
        ];

        echo "Mise à jour de la table conversion_rooms \n";

        // clean db
        DB::table('conversion_rooms')->delete();

        // parse && add in db
        for ($i = 0, $iMax = count($rooms); $i < $iMax; $i++)
        {
            // code hyp
            $key_hyperplanning = $rooms[$i];

            if( is_array($names) && array_key_exists($i, $names))
            {
                // nom salles
                $room_name = $names[$i];

                // prefix implantation
                $implantation_prefix = substr($room_name, 0, 2);

                // name implantation
                if (array_key_exists($implantation_prefix, $implantations_name_ref))
                    $implantation_name = $implantations_name_ref[$implantation_prefix];
                else
                    $implantation_name = '';

                // id implantation
                if (array_key_exists($implantation_prefix, $implantations_name_ref))
                    $implantation_id  = $implantations_id_ref[$implantation_prefix];
                else
                    $implantation_id = '';

                $conversion_room = new ConversionRoom();
                $conversion_room->key_hyperplanning = $key_hyperplanning;
                $conversion_room->room_name = $room_name;
                $conversion_room->implantation_prefix = $implantation_prefix;
                $conversion_room->implantation_name = $implantation_name;
                $conversion_room->implantation_id = $implantation_id;
                $conversion_room->save();

                echo "Ajout {$conversion_room->room_name} \n";
            }
            else
            {
                \Log::stack(['stack', 'conversion'])->error("[UpdateConversionRoom::handle] UCRh01 : Error missing room name in HP", [
                    'key_hyperplanning' => $key_hyperplanning,
                    'key' => $i,
                ]);
            }
        }
    }
}
