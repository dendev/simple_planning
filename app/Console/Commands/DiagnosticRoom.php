<?php

namespace App\Console\Commands;

use App\Console\Commands\Diagnostics\Util;
use Illuminate\Console\Command;

class DiagnosticRoom extends Command
{
    use Util;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple_planning:diagnostic_room {name? : room name LIKE IE-IG-1B-D} {--f|fake : dont get icla}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Check if room proecohenallux exist in HP";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // refs from hp
        \DiagnosticManager::init_room_refs();

        // is fake
        $is_fake = $this->option('fake') === true;

        // is single
        $is_single = ! empty($this->argument('name')) ;

        // basic
        $infos = [];
        $debugs = [];

        // get and check
        $name = $this->argument('name');
        $rooms = \DiagnosticManager::room_get_rooms($name);
        \DiagnosticManager::room_check_rooms_exist($rooms);

        // each room
        foreach( $rooms as $index => $room )
        {
            $this->info("\n* Travail sur room {$room->room_label}");

            // diagnostic
            \DiagnosticManager::room_run_diagnostic($room, $is_fake);

            // get current info && debug
            $info = \DiagnosticManager::room_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::room_get_debug();

            // quick info
            $this->_brief(\DiagnosticManager::room_is_valid($info, $is_fake ) );
        }

        // if no rooms
        if( count( $rooms) == 0 )
        {
            \DiagnosticManager::room_init_debug_infos();
            $info = \DiagnosticManager::room_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::room_get_debug();
        }

        // conclusion
        $this->info("\n** Conclusion");
        $results = \DiagnosticManager::room_get_results($infos, $debugs, $is_fake);
        $this->_output_results($results, $is_single);

        // remark ( help )
        $this->info("\n* Remarque");
        $this->_remark();
    }

    private function _remark()
    {
        $this->info("** L'affichage de l'horaire portail dépend du nombr d'heure à afficher par le portail.");
        $this->info("** Le lien entre proeco et hyperplanning se fait via la création de tables de conversions exécuté toutes les 24h");
        $this->info("\n");
    }
}
