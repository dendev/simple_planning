<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\Commands\Diagnostics\Util;

class DiagnosticStudent extends Command
{
    use Util;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple_planning:diagnostic_student {numproeco? : student num} {--f|fake : no ical} {--r|raw : return output as array}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if student has a planning';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // refs from hp
        \DiagnosticManager::init_promotion_refs();

        // is fake
        $is_fake = $this->option('fake') === true;

        // is single
        $is_single = ! empty($this->argument('numproeco')) ;

        // basic
        $infos = [];
        $debugs = [];

        // get and check
        $numproeco = $this->argument('numproeco');
        $students = \DiagnosticManager::student_get_students($numproeco);
        \DiagnosticManager::student_check_students_exist($students);

        // each student
        foreach( $students as $index => $student )
        {
            $this->info("\n* Travail sur etu numproeco {$student->numproeco}");

            // diagnostic
            \DiagnosticManager::student_run_diagnostic($student, $is_fake);

            // get current info && debug
            $info = \DiagnosticManager::student_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::student_get_debug();

            // quick info
            $this->_brief(\DiagnosticManager::student_is_valid( $info, $is_fake ) );
        }

        // if no students
        if( count( $students ) == 0 )
        {
            \DiagnosticManager::student_init_debug_infos();
            $info = \DiagnosticManager::student_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::student_get_debug();
        }

        // conclusion
        $this->info("\n** Conclusion");
        $results = \DiagnosticManager::student_get_results($infos, $debugs, $is_fake);
        $this->_output_results($results, $is_single);

        // remark ( help )
        $this->info("\n* Remarque");
        $this->_remark();
    }

    private function _remark()
    {
        $this->info("** L'affichage de l'horaire portail dépend du nombr d'heure à afficher par le portail.");
        $this->info("** Le lien entre proeco et hyperplanning se fait via la création de tables de conversions exécuté toutes les 24h");
        $this->info("\n");
    }
}
