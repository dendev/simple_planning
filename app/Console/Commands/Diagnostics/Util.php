<?php

namespace App\Console\Commands\Diagnostics;

trait Util
{
    private function _brief($is_valid)
    {
        if(  $is_valid )
            $this->info('* Ok');
        else
            $this->info('* KO');
    }

    private function _output_results($results, $is_single)
    {
        $valids = $results['valids'];
        $errors = $results['errors'];

        if(  $is_single )
        {
            $this->_output_infos($valids);
            $this->_output_infos($errors);
        }
        else
        {
            $this->_output_infos($errors);
        }
    }

    private function _output_infos($infos)
    {
        foreach( $infos as $info )
        {
            $debug_str = '';
            foreach( $info as $type => $inf)
            {
                if( $type === 'debug')
                {
                    $debug_str = json_encode($inf, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                }
                else
                {
                    // fr
                    $is_valid = $inf['is_valid'];
                    $is_valid_fr = $is_valid ? 'Oui' : 'Non';
                    $is_valid_prefix = $is_valid ? '++' : '--';

                    // show
                    $this->info("\n** {$inf['label']}");
                    $this->info("$is_valid_prefix est en ordre: $is_valid_fr");
                    $this->info("$is_valid_prefix {$inf['msg']}");

                }
            }

            // debug
            $this->info("\n!! Debug");
            $this->info("$debug_str");
        }

        $total = count($infos);
        if( $total > 0 )
            $this->info("\n** Total $total");
    }
}
