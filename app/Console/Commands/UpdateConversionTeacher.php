<?php

namespace App\Console\Commands;

use App\Models\ConversionTeacher;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateConversionTeacher extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple_planning:update_conversion_teacher';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate conversion_teachers table with promotions datas from hyperplanning and proeco';

    /**
     * The client for hyperplanning service
     *
     * @var Object SoapClient
     */
    protected $client;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        echo "Mise à jour de la table hyperplanning_teachers\n";

        // remove
        DB::table('conversion_teachers')->delete();

        // get datas && save
        $keys_teacher = \PlanningManager::tousLesEnseignants();
        foreach( $keys_teacher as $key)
        {
            // gets name, first name, code from hyperplanning
            $name = \PlanningManager::nomEnseignant($key);
            $firstname = \PlanningManager::prenomEnseignant($key);
            $code = \PlanningManager::codeEnseignant($key);
            dump( $key );
            dump( $name );
            dump( $firstname );
            dump( $code );

            // code to login
            $login = 'mdp' . strtolower($code);
            dump( $login );

            // use name && first name to get email && numproeco from proecohenallux
            $sql = DB::connection('proecohenallux' )->select("
                           SELECT numproeco AS numproeco, login AS login, CONCAT( login, '@henallux.be' ) AS email
                           FROM proecohenallux.profsproeco
                           WHERE login = ?
                           AND actif = '1';
                           "
                ,[$login]);

            if( $sql )
            {
                $conversion_teacher = new ConversionTeacher();
                $conversion_teacher->name = $name;
                $conversion_teacher->firstname = $firstname;
                $conversion_teacher->numproeco = $sql[0]->numproeco;
                $conversion_teacher->login = $sql[0]->login;
                $conversion_teacher->email = $sql[0]->email;
                $conversion_teacher->code_hyperplanning = $code;
                $conversion_teacher->key_hyperplanning = $key;
                $conversion_teacher->save();

                echo "Ajout {$conversion_teacher->numproeco} {$conversion_teacher->login} \n";
            }
            else
            {
                \Log::stack(['stack', 'conversion'])->error("[UpdateConversionTeacher::handle] UCTh01 : Teacher not found in proecohenallux", [
                    'login' => $login,
                ]);
            }
        }
    }
}
