<?php

namespace App\Console\Commands;

use App\Models\ConversionExterne;
use App\Models\MdpHyperplanning;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateConversionExterne extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple_planning:update_conversion_externe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate converstion_externes with user datas from hyperplanning';

    /**
     * The client for hyperplanning service
     *
     * @var Object SoapClient
     */
    protected $client;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Mise à jour de la table conversion_externs\n";

        // remove
        DB::table('conversion_externes')->delete();

        // get datas && save
       	 $keys_teacher = \PlanningManager::tousLesEnseignants();

        foreach( $keys_teacher as $key)
        {
            // gets name, first name, code from hyperplanning
            $name = \PlanningManager::nomEnseignant($key);
            $firstname = \PlanningManager::prenomEnseignant($key);
            $code = \PlanningManager::codeEnseignant($key);

            // code to login
            $mdp_login = 'mdp' . strtolower($code);
            $ext_login = 'ext' . strtolower($code);

            // debug
            dump( $key );
            dump( $name );
            dump( $firstname );
            dump( $code );
            dump( $ext_login );

            // use name && first name to get email && numproeco from proecohenallux
            $sql = DB::connection('proecohenallux' )->select("
                           SELECT numproeco AS numproeco, login AS login, CONCAT( login, '@henallux.be' ) AS email
                           FROM proecohenallux.profsproeco
                           WHERE login = ?
                           AND actif = '1';
                           "
                ,[$mdp_login]);

            if( ! $sql  && $code )
            {
                $externe = $this->get_sheldon_externe_by_abbreviation($code);
                if( $externe )
                {
                    $email_ad = $externe->alias . '@henallux.be';

                    $conversion_externe = new ConversionExterne();
                    $conversion_externe->name = $name;
                    $conversion_externe->firstname = $firstname;
                    $conversion_externe->id_people = $externe->id_people;
                    $conversion_externe->login = $ext_login;
                    $conversion_externe->email = $email_ad;
                    $conversion_externe->code_hyperplanning = $code;
                    $conversion_externe->key_hyperplanning = $key;
                    $conversion_externe->save();
                    echo "Ajout {$conversion_externe->id_people} {$conversion_externe->ext_login} \n";
                }
                else
                {
                    \Log::stack(['stack', 'conversion'])->error("[UpdateConversionExterne::handle] UCEh02 : Externe not found in sheldon", [
                        'ext_login' => $ext_login,
                        'code' => $code,
                    ]);
                }
            }
            else if( ! $code )
            {
                \Log::stack(['stack', 'conversion'])->error("[UpdateConversionExterne::handle] UCEh01 : Externe without HP code", [
                    'ext_login' => $ext_login,
                    'code' => $code,
                ]);
            }
        }
    }

    public function get_sheldon_externe_by_abbreviation($abbreviation)
    {
        $abbreviation = strtolower($abbreviation);
        $sql = "SELECT * FROM externes_he WHERE abreviation = ?";
        $externe = DB::connection('sheldon')->select($sql, [$abbreviation]);

        if( ! empty($externe ) )
        {
            $first = array_key_first($externe);
            $externe = $externe[$first];
        }

        return $externe;
    }
}
