<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\Commands\Diagnostics\Util;

class DiagnosticExtern extends Command
{
    use Util;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple_planning:diagnostic_extern {login? : extern login} {--f|fake : no ical}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if extern has a planning';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // refs from hp
        \DiagnosticManager::init_extern_refs();

        // is fake
        $is_fake = $this->option('fake') === true;

        // is single
        $is_single = ! empty($this->argument('login')) ;

        // basic
        $infos = [];
        $debugs = [];

        // get and check
        $login = $this->argument('login');
        $externs = \DiagnosticManager::extern_get_externs($login);
        \DiagnosticManager::extern_check_externs_exist($externs);

        // each extern
        foreach( $externs as $index => $extern )
        {
            $extern->login = 'ext' . $extern->abreviation;
            $this->info("\n* Travail sur extern login : {$extern->login}");

            // diagnostic
            \DiagnosticManager::extern_run_diagnostic($extern, $is_fake);

            // get current info && debug
            $info = \DiagnosticManager::extern_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::extern_get_debug();

            // quick info
            $this->_brief(\DiagnosticManager::extern_is_valid( $info, $is_fake ) );
        }

        // if no extern
        if( count( $externs ) == 0 )
        {
            \DiagnosticManager::extern_init_debug_infos();
            $info = \DiagnosticManager::extern_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::extern_get_debug();
        }

        // conclusion
        $this->info("\n** Conclusion");
        $results = \DiagnosticManager::extern_get_results($infos, $debugs, $is_fake);
        $this->_output_results($results, $is_single);

        // remark ( help )
        $this->info("\n* Remarque");
        $this->_remark();
    }

    private function _remark()
    {
        $this->info("** L'affichage de l'horaire portail dépend du nombr d'heure à afficher par le portail.");
        $this->info("** Le lien entre proeco et hyperplanning se fait via la création de tables de conversions exécuté toutes les 24h");
        $this->info("\n");
    }
}
