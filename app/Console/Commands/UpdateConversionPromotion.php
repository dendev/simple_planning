<?php

namespace App\Console\Commands;

use App\Models\ConversionPromotion;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateConversionPromotion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simple_planning:update_conversion_promotion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate conversion_promotions table with promotions datas from hyperplanning and proeco';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        echo "Mise à jour de la table conversion_promotions\n";

        // remove
        DB::table('conversion_promotions')->delete();

        $keys_promotion = \PlanningManager::ToutesLesPromotions();
        $names_promotion = \PlanningManager::NomsTableauDePromotions($keys_promotion);

        $key_name_promotions = [];
        for($i = 0, $iMax = count($keys_promotion); $i < $iMax; $i++ ) // !! count keys always == count codes ?
        {
            $key = $keys_promotion[$i];
            $name = $names_promotion[$i];

            $key_name_promotions[$key] = $name;
        }

        // FIXME not classes but promotion
        $classes = DB::connection('proecohenallux')->select("
                SELECT cp.idClasse AS promotion_id, cp.intitule AS promotion_label, im.num as implantation_id, im.nomimpl AS implantation_label, oi.id AS orientation_id, CONCAT_WS( ' - ', oi.code, oi.intitule, oi.bib ) AS orientation_label, cp.annee, cp.classe
                FROM proecohenallux.classesproeco AS cp
                INNER JOIN orientations AS oi ON oi.id = cp.fkOrientation
                INNER JOIN implantation AS im ON im.num = cp.fkImplantation
                ");

        foreach( $classes as $classe )
        {
            if( $key = array_search( $classe->promotion_label, $key_name_promotions ) )
            {

                $classe->name_hyperplanning = $key_name_promotions[$key];
                $classe->key_hyperplanning = $key;
            }
            else
            {
                \Log::stack(['stack', 'conversion'])->error("[UpdateConversionPromotion::handle] UCPh01 : Missing hyperplanning name", [
                    'promotion_label' => $classe->promotion_label,
                ]);
            }

            $conversion_promotion = new ConversionPromotion( get_object_vars($classe) );
            $conversion_promotion->save();

            echo "Ajout {$conversion_promotion->name_hyperplanning} \n";
        }
    }
}
