<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      x={
 *          "logo": {
 *              "url": "https://via.placeholder.com/190x90.png?text=L5-Swagger"
 *          }
 *      },
 *      title="Simple Planning Api",
 *      description="api routes",
 *      @OA\Contact(
 *          email="mdpdevde@henallux.be"
 *      ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="https://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 */
class ApiController extends Controller
{
    public function __construct()
    {
      //  $this->middleware('auth:sanctum'); // hint ,['except' => ['route_name']]);
    }

    public function make_response($data)
    {
        // http return code
        $http_code = ( $data && ! empty( $data) ) ? 200 : 204;

        // format return
        return Response::json( $data, $http_code );
    }
}

/*
 * https://sathyaventhan.medium.com/laravel-secure-rest-api-with-swagger-26050dafa55
 * https://dev.to/avsecdongol/laravel-api-documentation-with-swagger-and-passport-3ec0
 */
