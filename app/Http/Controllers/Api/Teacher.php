<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Response;
use App\Traits\Util;


class Teacher extends ApiController
{
    use Util;

    /**
     * @OA\Get(
     *     path="/api/teachers/calendar/{keys}/{nb_weeks}",
     *     tags={"teachers"},
     *     summary="get teachers calendar",
     *     description="get teachers calendar as json for on or more teachers hp key ",
     *     operationId="get_teachers_calendar",
     *     @OA\Parameter(
     *         name="keys",
     *         in="path",
     *         description="json array of hp keys or unique value",
     *         required=true,
     *         @OA\Schema(
     *             default="['6433']",
     *             type="string",
     *             enum={"['6433']", "['6433', '7400']", "6433"},
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="nb_weeks",
     *         in="path",
     *         description="nb weeks to get",
     *         required=false,
     *         @OA\Schema(
     *             default=25,
     *             type="integer",
     *             enum={25, 33, 53}
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Return calendar as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No calendar for this request"
     *     ),
     *     security={
     *         {"teachers_auth": {"write:teachers", "read:teachers"}}
     *     }
     * )
     */
    public function get_calendar($keys, $nb_weeks = 25)
    {
        $keys = $this->_format_api_keys($keys);

        $calendar = [];

        foreach( $keys as $key )
        {
            $ical = \PlanningManager::icalEnseignant($key, $nb_weeks);
            $calendar = $this->_convert_ical_to_array($ical, $calendar);
        }

        return $this->make_response($calendar);
    }

}
