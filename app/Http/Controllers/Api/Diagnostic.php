<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Artisan;
use App\Traits\Util;

class Diagnostic extends ApiController
{
    use Util;

     /**
     * @OA\Get(
     *     path="/api/diagnostic/promotions/{name}",
     *     tags={"diagnostic"},
     *     summary="get diagnostic of specific promotion",
     *     description="get diagnostic as json of the specified promotion",
     *     operationId="get_diagnostic_promotion_specific",
     *     @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="hp name of promotion",
     *         required=true,
     *         @OA\Schema(
     *             default="IE-DR-3B-E",
     *             type="string",
     *             enum={"PB-NSFR-1B-A", "CH-NPRES-3B-D", "IE-NSEFS-3B-C"},
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Return diagnostic as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No diagnostic for this request"
     *     ),
     *     security={
     *         {"diagnostic_auth": {"write:diagnostic", "read:diagnostic"}}
     *     }
     * )
     */
    public function get_promotion($name, $fake = false)
    {
        $infos = [];
        $debugs = [];

        // get promotion
        $promotions = \DiagnosticManager::promotion_get_promotions($name);
        \DiagnosticManager::promotion_check_promotions_exist($promotions);

        // init refs
        \DiagnosticManager::init_promotion_refs();

        // run diagnostic
        foreach( $promotions as $index => $promotion )
        {
            // diagnostic
            \DiagnosticManager::promotion_run_diagnostic($promotion, $fake);

            // get current info && debug
            $infos[] = \DiagnosticManager::promotion_get_info();
            $debugs[] = \DiagnosticManager::promotion_get_debug();
        }

        // if no promotions
        if( count( $promotions ) == 0 )
        {
            \DiagnosticManager::promotion_init_debug_infos();
            $info = \DiagnosticManager::promotion_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::promotion_get_debug();
        }

        // conclusion
        $results = \DiagnosticManager::promotion_get_results($infos, $debugs, $fake);

        return $this->make_response($results);

    }

     /**
     * @OA\Get(
     *     path="/api/diagnostic/promotions",
     *     tags={"diagnostic"},
     *     summary="get diagnostic of all promotions",
     *     description="get diagnostic as json for all promotions with error",
     *     operationId="get_diagnostic_promotion_all",
     *     @OA\Response(
     *         response=200,
     *         description="Return diagnostic as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No diagnostic for this request"
     *     ),
     *     security={
     *         {"diagnostic_auth": {"write:diagnostic", "read:diagnostic"}}
     *     }
     * )
     */
    public function get_promotions()
    {
        $infos = [];
        $debugs = [];

        // get student
        $promotions = \DiagnosticManager::promotion_get_promotions();
        \DiagnosticManager::promotion_check_promotions_exist($promotions);

        // init refs
        \DiagnosticManager::init_promotion_refs();

        // run diagnostic
        foreach( $promotions as $index => $promotion )
        {
            // diagnostic
            \DiagnosticManager::promotion_run_diagnostic($promotion, true);

            // get current info && debug
            $infos[] = \DiagnosticManager::promotion_get_info();
            $debugs[] = \DiagnosticManager::promotion_get_debug();
        }

        // if no promotions
        if( count( $promotions ) == 0 )
        {
            \DiagnosticManager::promotion_init_debug_infos();
            $info = \DiagnosticManager::promotion_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::promotion_get_debug();
        }

        // conclusion
        $results = \DiagnosticManager::promotion_get_results($infos, $debugs, true);

        return $this->make_response($results);
    }

    /**
     * @OA\Get(
     *     path="/api/diagnostic/students/{numproeco}",
     *     tags={"diagnostic"},
     *     summary="get diagnostic of specific student",
     *     description="get diagnostic as json of the specified student",
     *     operationId="get_diagnostic_student_specific",
     *     @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="hp name of student",
     *         required=true,
     *         @OA\Schema(
     *             default="24802",
     *             type="string",
     *             enum={"52403", "52941", "52906"},
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Return diagnostic as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No diagnostic for this request"
     *     ),
     *     security={
     *         {"diagnostic_auth": {"write:diagnostic", "read:diagnostic"}}
     *     }
     * )
     */
    public function get_student($numproeco, $fake = false)
    {
        $infos = [];
        $debugs = [];

        // get student
        $students = \DiagnosticManager::student_get_students($numproeco);
        \DiagnosticManager::student_check_students_exist($students);

        // init refs
        \DiagnosticManager::init_promotion_refs();

        // run diagnostic
        foreach( $students as $index => $student )
        {
            // diagnostic
            \DiagnosticManager::student_run_diagnostic($student, $fake);

            // get current info && debug
            $infos[] = \DiagnosticManager::student_get_info();
            $debugs[] = \DiagnosticManager::student_get_debug();
        }

         // if no students
        if( count( $students ) == 0 )
        {
            \DiagnosticManager::student_init_debug_infos();
            $info = \DiagnosticManager::student_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::student_get_debug();
        }


        // conclusion
        $results = \DiagnosticManager::student_get_results($infos, $debugs, $fake);

        return $this->make_response($results);
    }

   /**
     * @OA\Get(
     *     path="/api/diagnostic/students",
     *     tags={"diagnostic"},
     *     summary="get diagnostic of all students",
     *     description="get diagnostic as json for all students with error",
     *     operationId="get_diagnostic_student_all",
     *     @OA\Response(
     *         response=200,
     *         description="Return diagnostic as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No diagnostic for this request"
     *     ),
     *     security={
     *         {"diagnostic_auth": {"write:diagnostic", "read:diagnostic"}}
     *     }
     * )
     */
    public function get_students()
    {
        $infos = [];
        $debugs = [];

        // get student
        $students = \DiagnosticManager::student_get_students();
        \DiagnosticManager::student_check_students_exist($students);

        // init refs
        \DiagnosticManager::init_promotion_refs();

        // run diagnostic
        foreach( $students as $index => $student )
        {
            // diagnostic
            \DiagnosticManager::student_run_diagnostic($student, true);

            // get current info && debug
            $infos[] = \DiagnosticManager::student_get_info();
            $debugs[] = \DiagnosticManager::student_get_debug();
        }

        // if no students
        if( count( $students ) == 0 )
        {
            \DiagnosticManager::student_init_debug_infos();
            $info = \DiagnosticManager::student_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::student_get_debug();
        }

        // conclusion
        $results = \DiagnosticManager::student_get_results($infos, $debugs, true);

        return $this->make_response($results);
    }

    /**
     * @OA\Get(
     *     path="/api/diagnostic/teachers/{login}",
     *     tags={"diagnostic"},
     *     summary="get diagnostic of specific student",
     *     description="get diagnostic as json of the specified student",
     *     operationId="get_diagnostic_teacher_specific",
     *     @OA\Parameter(
     *         name="login",
     *         in="path",
     *         description="login name as mdpdevde",
     *         required=true,
     *         @OA\Schema(
     *             default="mdpdevde",
     *             type="string",
     *             enum={"mdpabsje", "mdpcolch", "mdpfralu"},
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Return diagnostic as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No diagnostic for this request"
     *     ),
     *     security={
     *         {"diagnostic_auth": {"write:diagnostic", "read:diagnostic"}}
     *     }
     * )
     */
    public function get_teacher($login, $fake = false)
    {
        $infos = [];
        $debugs = [];

        // get student
        $teachers = \DiagnosticManager::teacher_get_teachers($login);
        \DiagnosticManager::teacher_check_teachers_exist($teachers);

        // init refs
        \DiagnosticManager::init_teacher_refs();

        // run diagnostic
        foreach( $teachers as $index => $teacher )
        {
            // diagnostic
            \DiagnosticManager::teacher_run_diagnostic($teacher, $fake);

            // get current info && debug
            $infos[] = \DiagnosticManager::teacher_get_info();
            $debugs[] = \DiagnosticManager::teacher_get_debug();
        }

        // if no teacher
        if( count( $teachers ) == 0 )
        {
            \DiagnosticManager::teacher_init_debug_infos();
            $info = \DiagnosticManager::teacher_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::teacher_get_debug();
        }

        // conclusion
        $results = \DiagnosticManager::teacher_get_results($infos, $debugs, $fake);

        return $this->make_response($results);
    }

    /**
     * @OA\Get(
     *     path="/api/diagnostic/teachers",
     *     tags={"diagnostic"},
     *     summary="get diagnostic of all teachers",
     *     description="get diagnostic as json for all teachers with error",
     *     operationId="get_diagnostic_teacher_all",
     *     @OA\Response(
     *         response=200,
     *         description="Return diagnostic as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No diagnostic for this request"
     *     ),
     *     security={
     *         {"diagnostic_auth": {"write:diagnostic", "read:diagnostic"}}
     *     }
     * )
     */
    public function get_teachers()
    {
        $infos = [];
        $debugs = [];

        // get teacher
        $teachers = \DiagnosticManager::teacher_get_teachers();
        \DiagnosticManager::teacher_check_teachers_exist($teachers);

        // init refs
        \DiagnosticManager::init_teacher_refs();

        // run diagnostic
        foreach( $teachers as $index => $teacher )
        {
            // diagnostic
            \DiagnosticManager::teacher_run_diagnostic($teacher, true);

            // get current info && debug
            $infos[] = \DiagnosticManager::teacher_get_info();
            $debugs[] = \DiagnosticManager::teacher_get_debug();
        }

        // if no teacher
        if( count( $teachers ) == 0 )
        {
            \DiagnosticManager::teacher_init_debug_infos();
            $info = \DiagnosticManager::teacher_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::teacher_get_debug();
        }

        // conclusion
        $results = \DiagnosticManager::teacher_get_results($infos, $debugs, true);

        return $this->make_response($results);
    }

     /**
     * @OA\Get(
     *     path="/api/diagnostic/rooms/{name}",
     *     tags={"diagnostic"},
     *     summary="get diagnostic of specific room",
     *     description="get diagnostic as json of the specified room",
     *     operationId="get_diagnostic_room_specific",
     *     @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="name of room",
     *         required=true,
     *         @OA\Schema(
     *             default="'PA-Salle B'",
     *             type="string",
     *             enum={"'Ecole sociale JPO'", "'PA - FC -CHRN'", "'PA-Salle B'"},
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Return diagnostic as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No diagnostic for this request"
     *     ),
     *     security={
     *         {"diagnostic_auth": {"write:diagnostic", "read:diagnostic"}}
     *     }
     * )
     */
    public function get_room($name, $fake = false)
    {
        $infos = [];
        $debugs = [];

        // get rooms
        $rooms = \DiagnosticManager::room_get_rooms($name);
        \DiagnosticManager::room_check_rooms_exist($rooms);

        // init refs
        \DiagnosticManager::init_room_refs();

        // run diagnostic
        foreach( $rooms as $index => $room )
        {
            // diagnostic
            \DiagnosticManager::room_run_diagnostic($room, $fake);

            // get current info && debug
            $infos[] = \DiagnosticManager::room_get_info();
            $debugs[] = \DiagnosticManager::room_get_debug();
        }

        // if no room
        if( count( $rooms ) == 0 )
        {
            \DiagnosticManager::room_init_debug_infos();
            $info = \DiagnosticManager::room_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::room_get_debug();
        }

        // conclusion
        $results = \DiagnosticManager::room_get_results($infos, $debugs, $fake);

        return $this->make_response($results);
    }

    /**
     * @OA\Get(
     *     path="/api/diagnostic/rooms",
     *     tags={"diagnostic"},
     *     summary="get diagnostic of all rooms",
     *     description="get diagnostic as json for all rooms with error",
     *     operationId="get_diagnostic_room_all",
     *     @OA\Response(
     *         response=200,
     *         description="Return diagnostic as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No diagnostic for this request"
     *     ),
     *     security={
     *         {"diagnostic_auth": {"write:diagnostic", "read:diagnostic"}}
     *     }
     * )
     */
    public function get_rooms()
    {
        $infos = [];
        $debugs = [];

        // get room
        $rooms = \DiagnosticManager::room_get_rooms();
        \DiagnosticManager::room_check_rooms_exist($rooms);

        // init refs
        \DiagnosticManager::init_room_refs();

        // run diagnostic
        foreach( $rooms as $index => $room )
        {
            // diagnostic
            \DiagnosticManager::room_run_diagnostic($room, true);

            // get current info && debug
            $infos[] = \DiagnosticManager::room_get_info();
            $debugs[] = \DiagnosticManager::room_get_debug();
        }

        // if no room
        if( count( $rooms ) == 0 )
        {
            \DiagnosticManager::room_init_debug_infos();
            $info = \DiagnosticManager::room_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::room_get_debug();
        }

        // conclusion
        $results = \DiagnosticManager::room_get_results($infos, $debugs, true);

        return $this->make_response($results);
    }

    /**
     * @OA\Get(
     *     path="/api/diagnostic/externs/{login}",
     *     tags={"diagnostic"},
     *     summary="get diagnostic of specific extern",
     *     description="get diagnostic as json of the specified extern",
     *     operationId="get_diagnostic_extern_specific",
     *     @OA\Parameter(
     *         name="login",
     *         in="path",
     *         description="login name of extern",
     *         required=true,
     *         @OA\Schema(
     *             default="extchabe",
     *             type="string",
     *             enum={"extgodna", "extderbe", "extchabe"},
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Return diagnostic as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No diagnostic for this request"
     *     ),
     *     security={
     *         {"diagnostic_auth": {"write:diagnostic", "read:diagnostic"}}
     *     }
     * )
     */
    public function get_extern($login, $fake = false)
    {
        $infos = [];
        $debugs = [];

        // get student
        $externs = \DiagnosticManager::extern_get_externs($login);
        \DiagnosticManager::extern_check_externs_exist($externs);

        // init refs
        \DiagnosticManager::init_extern_refs();

        // run diagnostic
        foreach( $externs as $index => $extern )
        {
            // diagnostic
            \DiagnosticManager::extern_run_diagnostic($extern, $fake);

            // get current info && debug
            $infos[] = \DiagnosticManager::extern_get_info();
            $debugs[] = \DiagnosticManager::extern_get_debug();
        }

        // if no extern
        if( count( $externs ) == 0 )
        {
            \DiagnosticManager::extern_init_debug_infos();
            $info = \DiagnosticManager::extern_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::extern_get_debug();
        }

        // conclusion
        $results = \DiagnosticManager::extern_get_results($infos, $debugs, $fake);

        return $this->make_response($results);
    }

   /**
     * @OA\Get(
     *     path="/api/diagnostic/externs",
     *     tags={"diagnostic"},
     *     summary="get diagnostic of all externs",
     *     description="get diagnostic as json for all externs with error",
     *     operationId="get_diagnostic_extern_all",
     *     @OA\Response(
     *         response=200,
     *         description="Return diagnostic as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No diagnostic for this request"
     *     ),
     *     security={
     *         {"diagnostic_auth": {"write:diagnostic", "read:diagnostic"}}
     *     }
     * )
     */
    public function get_externs()
    {
        $infos = [];
        $debugs = [];

        // get extern
        $externs = \DiagnosticManager::extern_get_externs();
        \DiagnosticManager::extern_check_externs_exist($externs);

        // init refs
        \DiagnosticManager::init_extern_refs();

        // run diagnostic
        foreach( $externs as $index => $extern )
        {
            // diagnostic
            \DiagnosticManager::extern_run_diagnostic($extern, true);

            // get current info && debug
            $infos[] = \DiagnosticManager::extern_get_info();
            $debugs[] = \DiagnosticManager::extern_get_debug();
        }

        // if no extern
        if( count( $externs ) == 0 )
        {
            \DiagnosticManager::extern_init_debug_infos();
            $info = \DiagnosticManager::extern_get_info();
            $infos[] = $info;
            $debugs[] = \DiagnosticManager::extern_get_debug();
        }

        // conclusion
        $results = \DiagnosticManager::extern_get_results($infos, $debugs, true);

        return $this->make_response($results);
    }
}
