<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Response;
use App\Traits\Util;

class Extern extends ApiController
{
    use Util;

    /**
     * @OA\Get(
     *     path="/api/externs/calendar/{keys}/{nb_weeks}",
     *     tags={"externs"},
     *     summary="get externs calendar",
     *     description="get externs calendar as json for on or more externs hp key ",
     *     operationId="get_externs_calendar",
     *     @OA\Parameter(
     *         name="keys",
     *         in="path",
     *         description="json array of hp keys or unique value",
     *         required=true,
     *         @OA\Schema(
     *             default="['6433']",
     *             type="string",
     *             enum={"['6433']", "['6433', '7400']", "6433"},
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="nb_weeks",
     *         in="path",
     *         description="nb weeks to get",
     *         required=false,
     *         @OA\Schema(
     *             default=25,
     *             type="integer",
     *             enum={25, 33, 53}
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Return calendar as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No calendar for this request"
     *     ),
     *     security={
     *         {"externs_auth": {"write:externs", "read:externs"}}
     *     }
     * )
     */
    public function get_calendar($keys, $nb_weeks = 25)
    {
        $keys = $this->_format_api_keys($keys);

        $calendar = [];

        foreach( $keys as $key )
        {
            $ical = \PlanningManager::icalEnseignant($key, $nb_weeks);
            $calendar = $this->_convert_ical_to_array($ical, $calendar);
        }

        return $this->make_response($calendar);
    }

}
