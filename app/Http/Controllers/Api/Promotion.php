<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Response;
use App\Traits\Util;


class Promotion extends ApiController
{
    use Util;

    /**
     * @OA\Get(
     *     path="/api/promotions/calendar/{keys}/{nb_weeks}",
     *     tags={"promotions"},
     *     summary="get promotions calendar",
     *     description="get promotions calendar as json for on or more promotion hp key ",
     *     operationId="get_promotions_calendar",
     *     @OA\Parameter(
     *         name="keys",
     *         in="path",
     *         description="json array of hp keys or unique value",
     *         required=true,
     *         @OA\Schema(
     *             default="['5689']",
     *             type="string",
     *             enum={"['5689']", "['5689', '1110']", "5689"},
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="nb_weeks",
     *         in="path",
     *         description="nb weeks to get",
     *         required=false,
     *         @OA\Schema(
     *             default=25,
     *             type="integer",
     *             enum={25, 33, 53}
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Return calendar as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No calendar for this request"
     *     ),
     *     security={
     *         {"promotions_auth": {"write:promotions", "read:promotions"}}
     *     }
     * )
     */
    public function get_calendar($keys, $nb_weeks = 25)
    {
        $keys = $this->_format_api_keys($keys);

        $calendar = [];

        foreach( $keys as $key )
        {
            $ical = \PlanningManager::icalPromotion($key, $nb_weeks);
            $calendar = $this->_convert_ical_to_array($ical, $calendar);
        }

        return $this->make_response($calendar);
    }
}
