<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Response;
use App\Traits\Util;


class Room extends ApiController
{
    use Util;
/**
     * @OA\Get(
     *     path="/api/rooms/calendar/{keys}/{nb_weeks}",
     *     tags={"rooms"},
     *     summary="get rooms calendar",
     *     description="get rooms calendar as json for on or more rooms hp key ",
     *     operationId="get_rooms_calendar",
     *     @OA\Parameter(
     *         name="keys",
     *         in="path",
     *         description="json array of hp keys or unique value",
     *         required=true,
     *         @OA\Schema(
     *             default="['4999']",
     *             type="string",
     *             enum={"['4999']", "['4999', '8640']", "4999"},
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="nb_weeks",
     *         in="path",
     *         description="nb weeks to get",
     *         required=false,
     *         @OA\Schema(
     *             default=25,
     *             type="integer",
     *             enum={25, 33, 53}
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Return calendar as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No calendar for this request"
     *     ),
     *     security={
     *         {"rooms_auth": {"write:rooms", "read:rooms"}}
     *     }
     * )
     */
    public function get_calendar($keys, $nb_weeks = 25)
    {
        $keys = $this->_format_api_keys($keys);

        $calendar = [];

        foreach( $keys as $key )
        {
            $ical = \PlanningManager::icalSalle($key, $nb_weeks);
            $calendar = $this->_convert_ical_to_array($ical, $calendar);
        }

        return $this->make_response($calendar);
    }

    /**
     * @OA\Get(
     *     path="/api/rooms/ical/{key}/{nb_weeks}",
     *     tags={"rooms"},
     *     summary="get rooms calendar",
     *     description="get rooms calendar as json for on or more rooms hp key ",
     *     operationId="get_rooms_ical",
     *     @OA\Parameter(
     *         name="key",
     *         in="path",
     *         description="hp key of room",
     *         required=true,
     *         @OA\Schema(
     *             default="4999",
     *             type="string",
     *             enum={"4999", "8640", "8773"},
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="nb_weeks",
     *         in="path",
     *         description="nb weeks to get",
     *         required=false,
     *         @OA\Schema(
     *             default=25,
     *             type="integer",
     *             enum={25, 33, 53}
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Return calendar as json",
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No calendar for this request"
     *     ),
     *     security={
     *         {"rooms_auth": {"write:rooms", "read:rooms"}}
     *     }
     * )
     */
    public function get_ical($key, $nb_weeks = 25)
    {
        $ical = \PlanningManager::icalSalle($key, $nb_weeks);
        return $this->make_response($ical);
    }

}
